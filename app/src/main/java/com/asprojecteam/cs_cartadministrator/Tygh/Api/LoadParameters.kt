package com.asprojecteam.cs_cartadministrator.Tygh.Api

import com.google.gson.annotations.SerializedName

class LoadParameters (
    @SerializedName("page") var page: Int = 0,
    @SerializedName("items_per_page") var itemsPerPage: Int = 0
) {
    @SerializedName("total_items")
    var totalItems: Int? = 0

    @SerializedName("amount_to")
    var amountTo: Int? = null

    @SerializedName("user_type")
    var userType: String? = ""

    @SerializedName("parent_id")
    var parentId: Int? = 0

    @SerializedName("q")
    var query: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("category_ids")
    var categoryIds: MutableList<Int>? = null
}