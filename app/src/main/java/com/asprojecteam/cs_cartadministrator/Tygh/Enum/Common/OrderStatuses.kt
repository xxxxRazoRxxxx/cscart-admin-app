package com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common

object OrderStatuses {
    const val STATUS_OPEN = "O"
    const val STATUS_COMPLETE = "C"
}