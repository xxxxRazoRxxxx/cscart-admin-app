package com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.asprojecteam.cs_cartadministrator.Classes.Category
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentCategories
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.ParamIds

class CategoriesAdapter (
    var items: MutableList<Category>,
    var params: LoadParameters,
    private val fragment: FragmentCategories
) : RecyclerView.Adapter<CategoriesAdapter.CategoriesHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = CategoriesHolder(LayoutInflater.from(parent.context).inflate(R.layout.fragment_categories_element, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: CategoriesHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class CategoriesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val categoryNameTextView = itemView.findViewById<TextView>(R.id.category_name)
        private val categoryProductsCount = itemView.findViewById<TextView>(R.id.category_products_count)

        private val cardView = itemView.findViewById<CardView>(R.id.category_cardview)
        private val nextPageLoader = itemView.findViewById<ProgressBar>(R.id.category_nextpage_loader)

        fun bind(item: Category) {
            if (item.id == ParamIds.NEXT_PAGE_ID) {
                cardView.visibility = View.GONE
                nextPageLoader.visibility = View.VISIBLE

                val isSearch = fragment.categoriesList.adapter !== fragment.adapter

                fragment.mController.loadCategories(params.page + 1, params, isSearch)
            } else {
                cardView.visibility = View.VISIBLE
                nextPageLoader.visibility = View.GONE

                categoryNameTextView.text = item.category
                categoryProductsCount.text = item.productCount.toString()

                if (!item.isChecked) {
                    cardView.setCardBackgroundColor(ContextCompat.getColor(fragment.requireContext(), R.color.cardWhiteBackground))
                } else {
                    cardView.setCardBackgroundColor(ContextCompat.getColor(fragment.requireContext(), R.color.cardSelectedBackground))
                }
            }

            itemView.setOnClickListener {
                fragment.mController.onCategoryClicked(item, cardView)
            }
        }
    }

    /**
     * Adds loaded categories to the categories list
     *
     * @param newItems  List of the loaded categories
     * @param newParams Params of the loaded categories
     */
    fun addLoadedCategories(newItems: List<Category>, newParams: LoadParameters)
    {
        params = newParams

        items.removeAt(items.size - 1)
        items.addAll(newItems)

        notifyItemRangeChanged(items.size - newItems.count(), newItems.count())
    }
}