package com.asprojecteam.cs_cartadministrator.Tygh.InterfaceManager

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar

/**
 * Interface to manage via errors, load and etc components
 */
abstract class AInterfaceManager {

    lateinit var viewer: RelativeLayout
    lateinit var mainContent: LinearLayout
    lateinit var viewerNoData: RelativeLayout
    lateinit var loader: ProgressBar
    lateinit var context: Context

    /**
     * Register interface components
     *
     * @param Viewer       Main element of the fragment
     * @param ViewerNoData Element with error message
     * @param Loader       Loader element of the fragment
     * @param Context      Context of the fragment
     */
    open fun registrateComponents (
        Viewer: RelativeLayout,
        ViewerNoData: RelativeLayout,
        Loader: ProgressBar,
        Context: Context
    ) {
        viewer = Viewer
        viewerNoData = ViewerNoData
        loader = Loader
        context = Context
    }

    /**
     * Register interface components
     *
     * @param Viewer       Main element of the fragment
     * @param ViewerNoData Element with error message
     * @param Loader       Loader element of the fragment
     * @param Context      Context of the fragment
     */
    open fun registrateComponents (
        Viewer: LinearLayout,
        ViewerNoData: RelativeLayout,
        Loader: ProgressBar,
        Context: Context
    ) {
        mainContent = Viewer
        viewerNoData = ViewerNoData
        loader = Loader
        context = Context
    }

    /**
     * Show viewer
     */
    fun showViewer()
    {
        viewer.visibility = View.VISIBLE
    }

    /**
     * Show main content
     */
    fun showMainContent()
    {
        mainContent.visibility = View.VISIBLE
    }

    /**
     * Hide loader
     */
    fun hideLoader()
    {
        loader.visibility = View.GONE
    }

    /**
     * Hide an error
     */
    fun hideNoDataError()
    {
        viewerNoData.visibility = View.GONE
    }

    /**
     * Shows an error if the data wasn't loaded
     */
    open fun showNoDataError()
    {
        viewer.visibility = View.VISIBLE
        hideLoader()
    }

    /**
     * Shows loader progress bar while the data is loading
     */
    open fun showLoader()
    {
        if (this::viewer.isInitialized) {
            viewer.visibility = View.GONE
        }

        if (this::mainContent.isInitialized) {
            mainContent.visibility = View.GONE
        }

        loader.visibility = View.VISIBLE
        hideNoDataError()
    }

    /**
     * Show toast message
     *
     * @param message Message to show
     */
    fun showMessage(message: String)
    {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * Shows snackbar message
     */
    fun showSnackbarMessage(message: String)
    {
        Snackbar.make(viewer, message, Snackbar.LENGTH_SHORT)
            .setAction("Action", null).show()
    }
}