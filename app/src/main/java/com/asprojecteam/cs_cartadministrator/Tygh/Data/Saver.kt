package com.asprojecteam.cs_cartadministrator.Tygh.Data

import android.content.Context
import com.asprojecteam.cs_cartadministrator.Classes.OrderStatus

class Saver (
  private val type: String,
  private val mContext: Context
) {
    fun saveString(key: String, value: String)
    {
        val sPref = mContext.getSharedPreferences(type, Context.MODE_PRIVATE)
        val editor = sPref.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun saveBoolean(key: String, value: Boolean)
    {
        val sPref = mContext.getSharedPreferences(type, Context.MODE_PRIVATE)
        val editor = sPref.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun restoreString(key: String): String?
    {
        val sPref = mContext.getSharedPreferences(type, Context.MODE_PRIVATE)
        return sPref.getString(key, "")
    }

    fun restoreBoolean(key: String): Boolean?
    {
        val sPref = mContext.getSharedPreferences(type, Context.MODE_PRIVATE)
        return sPref.getBoolean(key, false)
    }

    fun saveOrdersStatuses(statusList: List<OrderStatus>)
    {
        val sPref = mContext.getSharedPreferences(type, Context.MODE_PRIVATE)
        val editor = sPref.edit()
        for (status in statusList) {
            editor.putString(status.status, status.description)
        }
        editor.apply()
    }
}