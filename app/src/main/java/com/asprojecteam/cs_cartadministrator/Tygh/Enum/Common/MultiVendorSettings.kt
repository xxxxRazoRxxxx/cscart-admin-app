package com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common

object MultiVendorSettings {
    var settings = arrayListOf<String>(
        "vendors_per_page",
        "apply_for_vendor",
        "create_vendor_administrator_account",
        "display_vendor"
    )
}