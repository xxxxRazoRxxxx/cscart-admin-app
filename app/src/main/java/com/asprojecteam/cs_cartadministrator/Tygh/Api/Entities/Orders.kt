package com.asprojecteam.cs_cartadministrator.Tygh.Api.Entities

import com.asprojecteam.cs_cartadministrator.Classes.Order
import com.asprojecteam.cs_cartadministrator.Classes.OrderStatus
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.google.gson.annotations.SerializedName


object Orders {
    class Response {
        @SerializedName("orders")
        var orderList: MutableList<Order>? = null

        @SerializedName("params")
        var params: LoadParameters? = null
    }

    class Body {
        @SerializedName("status")
        var status = ""

        @SerializedName("notify_user")
        var notifyUser = "1"

        @SerializedName("notify_department")
        var notifyDepartment = "1"

        @SerializedName("notify_vendor")
        var notifyVendor = "1"
    }

    class StatusesResponse {
        @SerializedName("statuses")
        var orderStatusList: MutableList<OrderStatus>? = null
    }
}