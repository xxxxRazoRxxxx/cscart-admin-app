package com.asprojecteam.cs_cartadministrator.Tygh.Api.Entities

import com.asprojecteam.cs_cartadministrator.Classes.ImagePair
import com.asprojecteam.cs_cartadministrator.Classes.Product
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.google.gson.annotations.SerializedName

object Products {
    class Response {
        @SerializedName("products")
        var productList: MutableList<Product>? = null

        @SerializedName("params")
        var params: LoadParameters? = null
    }

    class Body {
        var id = 0

        @SerializedName("product_code")
        var productCode: String? = null

        @SerializedName("status")
        var status: String? = null

        @SerializedName("product")
        var name: String? = null

        @SerializedName("price")
        var price: Double? = null

        @SerializedName("amount")
        var amount: Int? = null

        @SerializedName("main_pair")
        var mainPair: ImagePair? = null

        @SerializedName("image_pairs")
        var imagePairs: HashMap<String, ImagePair>? = null

        @SerializedName("category_ids")
        var categoryIds: MutableList<Int>? = null
    }
}