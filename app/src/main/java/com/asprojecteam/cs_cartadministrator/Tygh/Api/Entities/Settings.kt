package com.asprojecteam.cs_cartadministrator.Tygh.Api.Entities

import com.asprojecteam.cs_cartadministrator.Classes.Setting
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.google.gson.annotations.SerializedName

object Settings {
    class Response {
        @SerializedName("settings")
        var settingsList: MutableList<Setting>? = null

        @SerializedName("params")
        var params: LoadParameters? = null
    }
}