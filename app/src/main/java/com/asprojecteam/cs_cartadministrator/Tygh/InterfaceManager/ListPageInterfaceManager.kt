package com.asprojecteam.cs_cartadministrator.Tygh.InterfaceManager

import android.content.Context
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.miguelcatalan.materialsearchview.MaterialSearchView

class ListPageInterfaceManager: AInterfaceManager() {

    lateinit var searchView: MaterialSearchView

    fun registrateComponents(
        Viewer: RelativeLayout,
        ViewerNoData: RelativeLayout,
        Loader: ProgressBar,
        SearchView: MaterialSearchView,
        Context: Context
    ) {
        searchView = SearchView
        super.registrateComponents(Viewer, ViewerNoData, Loader, Context)
    }

    fun showList()
    {
        viewer.visibility = View.VISIBLE
        hideLoader()
        hideNoDataError()
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    fun onBackPressed(): Boolean
    {
        if (searchView.isSearchOpen) {
            searchView.closeSearch()
            return true
        }

        return false
    }

    private fun hideList()
    {
        viewer.visibility = View.GONE
    }

    override fun showNoDataError()
    {
        viewerNoData.visibility = View.VISIBLE
        hideList()
        hideLoader()
    }

    override fun showLoader()
    {
        loader.visibility = View.VISIBLE
        hideList()
        hideNoDataError()
    }
}