package com.asprojecteam.cs_cartadministrator.Tygh.JsonDeserializer

import com.asprojecteam.cs_cartadministrator.Classes.ImagePair
import com.google.gson.*
import java.lang.reflect.Type

class ImagePairImageDeserializer: JsonDeserializer<ImagePair> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): ImagePair? {
        return if (json is JsonObject) {
            Gson().fromJson(json, ImagePair::class.java)
        } else {
            null
        }
    }
}