package com.asprojecteam.cs_cartadministrator.Tygh.Enum

import android.content.Context
import com.asprojecteam.cs_cartadministrator.R

object ProductStatuses {
    const val ACTIVE = "A"
    const val HIDDEN = "H"
    const val DISABLED = "D"
    const val REQUIRES_APPROVAL = "R"
    const val DISAPPROVED = "X"

    fun getStatusDescription(context: Context, status: String): String
    {
        return when (status) {
            ACTIVE            -> context.resources.getString(R.string.product_status_active)
            HIDDEN            -> context.resources.getString(R.string.product_status_hidden)
            DISABLED          -> context.resources.getString(R.string.product_status_disabled)
            REQUIRES_APPROVAL -> context.resources.getString(R.string.product_status_requires_approval)
            DISAPPROVED       -> context.resources.getString(R.string.product_status_disapproved)
            else              -> ""
        }
    }
}