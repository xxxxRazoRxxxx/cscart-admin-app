package com.asprojecteam.cs_cartadministrator.Tygh.InterfaceBuilder

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.text.TextUtils
import android.view.Gravity
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import com.asprojecteam.cs_cartadministrator.Classes.Category
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters

class ItemElementWithRemoveButtonBuilder (
    val context: Context,
    val resources: Resources,
    private val itemsList: Map<Int, String>,
    val textSize: Float,
    val onRemoveButtonClicked: (RelativeLayout, Int) -> Unit
) {
    private var scale: Float = 0f

    init {
        scale = resources.displayMetrics.density
    }

    fun getElements(): MutableList<RelativeLayout>
    {
        val padding = (2 * scale + 0.5f).toInt()
        val margin = (2 * scale + 0.5f).toInt()

        val elements = mutableListOf<RelativeLayout>()

        itemsList.forEach {
            val relativeLayout: RelativeLayout = RelativeLayout(context)
            relativeLayout.setPadding(padding, 0, padding, 0)
            relativeLayout.gravity = Gravity.CENTER

            val relativeLayoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            relativeLayoutParams.marginStart = margin

            relativeLayout.layoutParams = relativeLayoutParams

            val itemRemoveImageButton = ImageButton(context)

            val itemRemoveImageButtonLayoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            itemRemoveImageButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_END)

            itemRemoveImageButton.layoutParams = itemRemoveImageButtonLayoutParams
            itemRemoveImageButton.setImageResource(R.drawable.icon_remove_button_18)
            itemRemoveImageButton.setPadding(padding, padding, padding, 0)
            itemRemoveImageButton.setBackgroundColor(Color.TRANSPARENT)

            val id = it.key
            itemRemoveImageButton.setOnClickListener { onRemoveButtonClicked(relativeLayout, id) }

            val itemNameTextView = TextView(context)

            val itemNameTextViewLayoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            itemNameTextViewLayoutParams.addRule(RelativeLayout.START_OF, itemRemoveImageButton.id)

            itemNameTextView.setPadding(padding, 0, padding, 0)
            itemNameTextView.textSize = textSize
            itemNameTextView.setLines(2)
            itemNameTextView.ellipsize = TextUtils.TruncateAt.END
            itemNameTextView.text = it.value
            itemNameTextView.layoutParams = itemNameTextViewLayoutParams

            relativeLayout.addView(itemRemoveImageButton)
            relativeLayout.addView(itemNameTextView)
            elements.add(relativeLayout)
        }

        return elements
    }
}