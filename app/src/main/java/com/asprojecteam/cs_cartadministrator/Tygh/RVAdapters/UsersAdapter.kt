package com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.asprojecteam.cs_cartadministrator.Classes.User
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentUserDetailed
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentUsers
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles.UserBundle
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.ParamIds
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.UserTypes

class UsersAdapter(
    var items: MutableList<User>,
    var params: LoadParameters,
    private val fragment: FragmentUsers
) : RecyclerView.Adapter<UsersAdapter.ProductsHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = ProductsHolder(LayoutInflater.from(parent.context).inflate(R.layout.fragment_users_element, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ProductsHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ProductsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val idText = itemView.findViewById<TextView>(R.id.user_id)
        private val nameText = itemView.findViewById<TextView>(R.id.user_name)
        private val phoneText = itemView.findViewById<TextView>(R.id.users_phone)
        private val emailText = itemView.findViewById<TextView>(R.id.user_email)
        private val usersCardView = itemView.findViewById<CardView>(R.id.users_cardview)
        private val usersNextPageLoader = itemView.findViewById<ProgressBar>(R.id.users_nextpage_loader)

        fun bind(item: User) {
            if (item.id == ParamIds.NEXT_PAGE_ID) {
                usersCardView.visibility = View.GONE
                usersNextPageLoader.visibility = View.VISIBLE

                val isSearch = when(params.userType) {
                    UserTypes.ADMIN    -> fragment.usersList.adapter != fragment.usersAdminAdapter
                    UserTypes.CUSTOMER -> fragment.usersList.adapter != fragment.usersCustomerAdapter
                    else               -> false
                }

                fragment.mController.loadUsers(params.page + 1, params.userType!!, isSearch)
            } else {
                idText.text = "${fragment.getString(R.string.id)} ${item.id}"
                nameText.text = item.getName()
                phoneText.text = item.phone
                emailText.text = item.email

                itemView.setOnClickListener {
                    onItemClicked(item)
                }
            }
        }
    }

    /**
     * Adds loaded users to the user list
     *
     * @param newItems  List of the loaded users
     * @param newParams Params of the loaded users
     */
    fun addLoadedUsers(newItems: List<User>, newParams:  LoadParameters)
    {
        params = newParams

        items.removeAt(items.size - 1)
        items.addAll(newItems)

        notifyItemRangeChanged(items.size - newItems.count(), newItems.count())
    }

    /**
     * Opens user detailed fragment
     *
     * @param item User data
     */
    fun onItemClicked(item: User){
        val bundle = Bundle()
        bundle.putInt(UserBundle.USER_ID, item.id)
        bundle.putString(UserBundle.USER_NAME, item.getName())

        val fragmentUserDetailed = FragmentUserDetailed()
        val fragmentTrans = fragment.requireActivity().supportFragmentManager

        fragmentUserDetailed.arguments = bundle

        fragmentTrans.beginTransaction()
            .replace(R.id.main_container, fragmentUserDetailed)
            .addToBackStack(null)
            .commit()
    }
}