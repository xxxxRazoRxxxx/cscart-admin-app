package com.asprojecteam.cs_cartadministrator.Tygh

import android.app.Activity
import android.app.Service
import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.util.*
import com.asprojecteam.cs_cartadministrator.R


/**
 * Object with default functions which can be called from several places.
 */
object Common
{
    /**
     * Makes phone vibration for [msec] for [context]
     *
     * @param msec - time to vibrate
     * @param context - Context to vibrate for
     */
    fun vibratePhone(msec: Long, context: Context)
    {
        val vibrator = context.getSystemService(Service.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(msec, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            vibrator.vibrate(msec)
        }
    }

    /**
     * Gets language of the phone
     *
     * @param context - context to get additional information
     *
     * @return Locale
     */
    fun getCurrentLocale(context: Context): Locale
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return context.resources.configuration.locales.get(0);
        } else {
            return context.resources.configuration.locale
        }
    }

    /**
     * Gets symbol of the currency
     *
     * @param currency
     * @param context
     *
     * @return String
     */
    fun getCurrencySymbol(currency: String, context: Context): String
    {
        return when(currency) {
            "USD" -> context.resources.getString(R.string.usd)
            "EUR" -> context.resources.getString(R.string.eur)
            "GBP" -> context.resources.getString(R.string.gbp)
            else -> currency
        }
    }

    /**
     * Hides keybord
     *
     * @param context
     * @param view
     */
    fun hideKeyboard(context: Context, view: View) {
        val imm: InputMethodManager =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}