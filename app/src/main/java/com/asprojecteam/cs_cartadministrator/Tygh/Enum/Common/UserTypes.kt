package com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common

object UserTypes {
    const val ADMIN = "A"
    const val CUSTOMER = "C"
    const val VENDOR = "V"
}