package com.asprojecteam.cs_cartadministrator.Tygh.Enum

object YesNo {
    const val YES = "Y"
    const val NO  = "N"
}