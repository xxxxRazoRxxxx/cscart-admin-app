package com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common

object ParamIds {
    const val NEXT_PAGE_ID = -5

    /** Errors */
    const val ERROR_FAILED_TO_LOAD = -5
}