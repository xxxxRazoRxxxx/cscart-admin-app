package com.asprojecteam.cs_cartadministrator.Tygh.Api.Entities

import com.asprojecteam.cs_cartadministrator.Classes.Category
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.google.gson.annotations.SerializedName

object Categories {
    class Response {
        @SerializedName("categories")
        var categoryList: MutableList<Category>? = null

        @SerializedName("params")
        var params: LoadParameters? = null
    }
}