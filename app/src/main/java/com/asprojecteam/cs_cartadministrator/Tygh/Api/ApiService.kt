package com.asprojecteam.cs_cartadministrator.Tygh.Api

import android.content.Context
import com.asprojecteam.cs_cartadministrator.Classes.*
import com.asprojecteam.cs_cartadministrator.Classes.Storefront
import com.asprojecteam.cs_cartadministrator.Tygh.Api.Entities.*
import com.asprojecteam.cs_cartadministrator.Tygh.Data.Saver
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Saver.Types
import com.asprojecteam.cs_cartadministrator.Tygh.JsonDeserializer.ImagePairImageDeserializer
import com.asprojecteam.cs_cartadministrator.Tygh.JsonDeserializer.PaymentInfoDeserializer
import okhttp3.Credentials
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import okhttp3.OkHttpClient
import retrofit2.Call
import com.google.gson.GsonBuilder
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface ApiService {

    @POST("api/auth/")
    fun auth(@Body authBody: Auth.Body): Call<Auth.Response>

    @GET("api/2.0/orders")
    fun getOrders(
        @Query("items_per_page") itemsPerPage: Int,
        @Query("page") page: Int,
        @Query("status") status: String?
    ): Call<Orders.Response>

    @GET("api/2.0/orders/{order_id}")
    fun getOrderById(@Path("order_id") orderId: Int): Call<Order>

    @PUT("api/2.0/orders/{order_id}")
    fun updateOrderById(@Path("order_id") orderId: Int, @Body orderBody: Orders.Body): Call<Order>

    @GET("api/4.0/sra_storefront")
    fun getCurrency(): Call<Storefront>

    @GET("api/2.0/products/")
    fun getProducts(
        @Query("items_per_page") itemsPerPage: Int,
        @Query("page") page: Int,
        @Query("amount_to") amountTo: Int?,
        @Query("q") query: String?
    ): Call<Products.Response>

    @GET("api/2.0/products/{product_id}")
    fun getProductById(@Path("product_id") productId: Int): Call<Product>

    @PUT("api/2.0/products/{product_id}")
    fun updateProductById(@Path("product_id") productId: Int, @Body productBody: Products.Body): Call<Product>

    @POST("api/2.0/products")
    fun createProduct(@Body productBody: Products.Body): Call<Product>

    @GET("api/2.0/categories")
    fun getCategories(
        @Query("items_per_page") itemsPerPage: Int,
        @Query("page") page: Int,
        @Query("parent_id") parentId: Int?,
        @Query("category_ids[]") categoryIds: MutableList<Int>?
    ): Call<Categories.Response>

    @GET("api/2.0/statuses")
    fun getOrderStatuses(@Query("lang_code") langCode: String): Call<Orders.StatusesResponse>

    @GET("api/2.0/users")
    fun getUsers(
        @Query("page") page: Int,
        @Query("items_per_page") itemsPerPage: Int,
        @Query("user_type") userType: String,
        @Query("name") name: String?
    ): Call<Users.Response>

    @GET("api/2.0/users/{user_id}")
    fun getUserById(@Path("user_id") userId: Int): Call<User>

    @PUT("api/2.0/users/{user_id}")
    fun updateUserById(@Path("user_id") userId: Int, @Body userBody: Users.Body): Call<User>

    @GET("api/2.0/settings")
    fun getSettings(
        @Query("page") page: Int,
        @Query("items_per_page") itemsPerPage: Int
    ): Call<Settings.Response>

    @GET("api/4.0/m_store_edition")
    fun getStoreEdition(): Call<StoreEdition.Response>

    /**
     * Factory class for convenient creation of the Api Service interface
     */
    object Factory {

        private const val CONNECTION_TIMEOUT: Long = 20

        private var api: ApiService? = null

        private var baseUrl: String? = null

        fun getApi(context: Context): ApiService?
        {
            if (api != null) {
                return api!!
            }

            val saver = Saver(Types.SETTINGS_LOGIN, context)
            if (saver.restoreString(Types.LOGIN_KEY_API) != "") {

                val loginData = Login(
                    saver.restoreString(Types.LOGIN_KEY_EMAIL)!!,
                    saver.restoreString(Types.LOGIN_KEY_API)!!,
                    saver.restoreString(Types.LOGIN_KEY_URL)!!
                )

                return create(loginData)
            }

            return null
        }

        fun create(loginData: Login): ApiService? {
            baseUrl = loginData.url

            if (baseUrl!![baseUrl!!.length-1] != '/') {
                baseUrl = "$baseUrl/"
            }

            val gson = GsonBuilder()
                .registerTypeAdapter(PaymentInfo::class.java, PaymentInfoDeserializer())
                .registerTypeAdapter(ImagePair::class.java, ImagePairImageDeserializer())
                .setLenient()
                .create()

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient().newBuilder()
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor {
                        val originalRequest = it.request()
                        val newRequest = originalRequest.newBuilder().header("Authorization", Credentials.basic(loginData.login, loginData.password)).build()
                        it.proceed(newRequest)
                }
                .build()

            val retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(baseUrl!!)
                .build()

            setApi(retrofit.create(ApiService::class.java))
            return api
        }

        fun getBaseUrl(): String {
            return baseUrl ?: ""
        }

        private fun setApi(Api: ApiService)
        {
            api = Api
        }
    }
}