package com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.asprojecteam.cs_cartadministrator.Classes.Product
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentProducts
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.ParamIds
import com.bumptech.glide.Glide

class ProductsAdapter(
    var items: MutableList<Product>,
    var params: LoadParameters,
    private val fragment: FragmentProducts
) : RecyclerView.Adapter<ProductsAdapter.ProductsHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = ProductsHolder(LayoutInflater.from(parent.context).inflate(R.layout.fragment_products_element, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ProductsHolder, position: Int) {
        holder.bind(items[position], position)
    }

    inner class ProductsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val codeText = itemView.findViewById<TextView>(R.id.product_code)
        private val nameText = itemView.findViewById<TextView>(R.id.product_name)
        private val priceText = itemView.findViewById<TextView>(R.id.product_price)
        private val amountText = itemView.findViewById<TextView>(R.id.product_amount)
        private val image = itemView.findViewById<ImageView>(R.id.product_image)

        private val productCardView = itemView.findViewById<CardView>(R.id.product_cartview)
        private val productNextPageLoader = itemView.findViewById<ProgressBar>(R.id.product_nextpage_loader)

        fun bind(item: Product, position: Int) {
            if (item.id == ParamIds.NEXT_PAGE_ID) {
                productCardView.visibility = View.GONE
                productNextPageLoader.visibility = View.VISIBLE

                val isSearch = fragment.productsList.adapter !== fragment.adapter

                fragment.mController.loadProducts(params.page + 1, params, isSearch)
            } else {
                productCardView.visibility = View.VISIBLE
                productNextPageLoader.visibility = View.GONE

                codeText.text = item.code
                nameText.text = item.product
                priceText.text = item.getPriceWithCurrency()
                amountText.text = "${item.amount}"

                if (item.mainPair != null) {
                    Glide.with(fragment.requireContext()).load(item.mainPair!!.detailed.image_path).into(image)
                } else {
                    Glide.with(fragment.requireContext())
                        .load("")
                        .placeholder(R.drawable.ic_no_picture)
                        .into(image)
                }
            }

            itemView.setOnClickListener {
                fragment.mController.onProductClicked(item, position)
            }
        }
    }

    /**
     * Adds loaded products to the product list
     *
     * @param newItems  List of the loaded products
     * @param newParams Params of the loaded products
     */
    fun addLoadedProducts(newItems: List<Product>, newParams: LoadParameters)
    {
        params = newParams

        items.removeAt(items.size - 1)
        items.addAll(newItems)

        notifyItemRangeChanged(items.size - newItems.count(), newItems.count())
    }

    fun changeProduct(product: Product, position: Int)
    {
        items[position] = product
        notifyItemChanged(position)
    }
}