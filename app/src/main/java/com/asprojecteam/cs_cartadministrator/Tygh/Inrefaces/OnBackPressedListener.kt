package com.asprojecteam.cs_cartadministrator.Tygh.Inrefaces

interface OnBackPressedListener {
    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    fun onBackPressed(): Boolean
}