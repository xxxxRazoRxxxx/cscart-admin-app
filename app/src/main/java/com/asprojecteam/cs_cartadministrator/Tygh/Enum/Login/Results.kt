package com.asprojecteam.cs_cartadministrator.Tygh.Enum.Login

object Results
{
    const val API_KEY = "api_key"
    const val EMAIL = "email"
    const val URL = "url"
    const val RESULT = "result"
}