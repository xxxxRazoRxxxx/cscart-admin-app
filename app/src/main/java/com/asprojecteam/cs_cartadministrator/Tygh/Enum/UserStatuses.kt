package com.asprojecteam.cs_cartadministrator.Tygh.Enum

import android.content.Context
import com.asprojecteam.cs_cartadministrator.R

object UserStatuses {
    const val ACTIVE = "A"
    const val DISABLED = "D"

    fun getStatusDescription(context: Context, status: String): String
    {
        return when (status) {
            ACTIVE            -> context.resources.getString(R.string.status_active)
            DISABLED          -> context.resources.getString(R.string.status_disabled)
            else              -> ""
        }
    }
}