package com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles

object ProductBundle {
    const val PRODUCT_ID   = "product_id"
    const val PRODUCT_NAME = "product_name"
}