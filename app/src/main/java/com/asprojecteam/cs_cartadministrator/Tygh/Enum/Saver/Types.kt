package com.asprojecteam.cs_cartadministrator.Tygh.Enum.Saver

object Types
{
    const val SETTINGS_LOGIN    = "login_settings"
    const val LOGIN_KEY_EMAIL   = "login"
    const val LOGIN_KEY_API     = "api"
    const val LOGIN_KEY_URL     = "website"
    const val LOGIN_SUCCESSFUL  = "successful_login"

    const val SETTINGS_CURRENCY = "currency_settings"
    const val CURRENCY_KEY      = "currency"
    const val CURRENCY_VALUE    = "currency_value"
}