package com.asprojecteam.cs_cartadministrator.Tygh.JsonDeserializer

import com.asprojecteam.cs_cartadministrator.Classes.PaymentInfo
import com.google.gson.*
import java.lang.reflect.Type

class PaymentInfoDeserializer: JsonDeserializer<PaymentInfo> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): PaymentInfo? {
        return if (json is JsonObject) {
            Gson().fromJson(json, PaymentInfo::class.java)
        } else {
            null
        }
    }
}