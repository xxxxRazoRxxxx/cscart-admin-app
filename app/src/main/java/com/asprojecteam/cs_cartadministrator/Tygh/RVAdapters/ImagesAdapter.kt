package com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.asprojecteam.cs_cartadministrator.Classes.Image
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentProductDetailed
import com.asprojecteam.cs_cartadministrator.R
import com.bumptech.glide.Glide

class ImagesAdapter(var items: MutableList<Image>, private val fragment: FragmentProductDetailed): RecyclerView.Adapter<ImagesAdapter.ImagesHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = ImagesHolder(LayoutInflater.from(parent.context).inflate(R.layout.fragment_product_detailed_image_element, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ImagesHolder, position: Int) {
        holder.bind(items[position], position)
    }

    inner class ImagesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val image = itemView.findViewById<ImageView>(R.id.product_image)
        private val cardView = itemView.findViewById<CardView>(R.id.product_image_card_view)

        fun bind(item: Image, position: Int) {
            if (position == 0) {
                Glide.with(fragment.requireContext())
                    .load("")
                    .placeholder(R.drawable.icon_add_button)
                    .into(image)

                cardView.setOnClickListener(fragment.mController.onAddImageButtonClicked())
            } else {
                Glide.with(fragment.requireContext())
                    .load(item.image_path)
                    .into(image)

                cardView.setOnLongClickListener(fragment.mController.onImageLongClicked())
            }
        }
    }
}