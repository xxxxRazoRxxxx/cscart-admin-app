package com.asprojecteam.cs_cartadministrator.Tygh

import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import com.asprojecteam.cs_cartadministrator.Classes.*
import com.asprojecteam.cs_cartadministrator.Classes.Storefront
import com.asprojecteam.cs_cartadministrator.Tygh.Api.ApiService
import com.asprojecteam.cs_cartadministrator.Tygh.Api.Entities.*
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.ParamIds
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

const val ITEMS_PER_PAGE = 20

/**
 * Class to load information from API
 */
class DataLoader (
    val context: Context
) {
    private var apiService = ApiService.Factory.getApi(context)

    /**
     * Loads list of orders from api/2.0/orders with params
     *
     * @param onLoadOrdersResponse - callback function on success
     * @param params - load params to get information from API
     */
    fun loadOrders(onLoadOrdersResponse: (MutableList<Order>, LoadParameters) -> Unit, params: LoadParameters)
    {
        val itemsPerPage = params.itemsPerPage
        val page = params.page
        val status = params.status

        apiService?.getOrders(itemsPerPage, page, status)?.enqueue(object : Callback<Orders.Response> {
            override fun onResponse(call: Call<Orders.Response>?, response: Response<Orders.Response>) {
                if (response.isSuccessful && response.body() != null) {
                    onLoadOrdersResponse(
                        response.body()!!.orderList!!,
                        response.body()!!.params!!
                    )
                } else {
                    Log.e(TAG, "getOrders: error ${response.code()}")
                }

            }

            override fun onFailure(call: Call<Orders.Response>?, t: Throwable) {
                Log.e(TAG, "getOrders: error ${t.message}")
            }
        })
    }

    /**
     * Loads order by id from api/2.0/orders/{id}\
     *
     * @param onLoadOrdersResponse - callback function on Success
     * @param onFail - callback function on Fail
     * @param orderId - orderId to load
     */
    fun loadOrderById(
        onLoadOrdersResponse: (Order) -> Unit,
        onFail: () -> Unit,
        orderId: Int
    ) {
        apiService?.getOrderById(orderId)?.enqueue(object: Callback<Order> {
            override fun onResponse(call: Call<Order>, response: Response<Order>) {
                if (response.isSuccessful && response.body() != null) {
                    onLoadOrdersResponse(response.body()!!)
                } else {
                    onFail()

                    Log.e(TAG, "getOrderById: error ${response.code()}")
                }
            }

            override fun onFailure(call: Call<Order>, t: Throwable) {
                onFail()

                Log.e(TAG, "getOrderById: error ${t.message}")
            }
        })
    }

    /**
     * Loads orders statuses description from api/2.0/statuses
     *
     * @param onLoadOrderStatuses - call back function on Success
     * @param params - load params to get information from API
     */
    fun loadOrderStatuses(
        onLoadOrderStatuses: (MutableList<OrderStatus>, LoadParameters) -> Unit,
        onFail: () -> Unit,
        params: LoadParameters
    ) {
        var langCode = Common.getCurrentLocale(context).toString()
        langCode = langCode.split('_')[0]

        apiService?.getOrderStatuses(langCode)?.enqueue(object: Callback<Orders.StatusesResponse> {
            override fun onResponse(call: Call<Orders.StatusesResponse>, response: Response<Orders.StatusesResponse>) {
                if (response.isSuccessful && response.body() != null) {
                    onLoadOrderStatuses(response.body()!!.orderStatusList!!, params)
                } else {
                    onFail()

                    Log.e(TAG, "getOrderStatuses: error ${response.code()}")
                }
            }

            override fun onFailure(call: Call<Orders.StatusesResponse>, t: Throwable) {
                onFail()

                Log.e(TAG, "getOrderStatuses: error ${t.message}")
            }
        })
    }

    /**
     * Loads list of products by the params from the api/2.0/products
     *
     * @param onLoadProductsResponse - callback function on success
     * @param onFail - callback function on fail
     * @param params - load params, to load information from API
     * @param isSearch - flag, true if it is Search form, false, if not
     */
    fun loadProducts(
        onLoadProductsResponse: (MutableList<Product>, LoadParameters, Boolean) -> Unit,
        onFail: () -> Unit,
        params: LoadParameters,
        isSearch: Boolean = false
    ) {
        val itemsPerPage = params.itemsPerPage
        val page = params.page
        val amountTo = params.amountTo
        val query = params.query
        apiService?.getProducts(itemsPerPage, page, amountTo, query)?.enqueue(object : Callback<Products.Response> {
            override fun onResponse(call: Call<Products.Response>?, response: Response<Products.Response>) {
                if (response.isSuccessful && response.body() != null) {
                    onLoadProductsResponse(
                        response.body()!!.productList!!,
                        response.body()!!.params!!,
                        isSearch
                    )
                } else {
                    onFail()

                    Log.e(TAG, "getProducts: error ${response.code()}")
                }
            }

            override fun onFailure(call: Call<Products.Response>?, t: Throwable) {
                onFail()
                Log.e(TAG, "getProducts: error ${t.message}")
            }
        })
    }

    /**
     * Loads product by Id from the api/2.0/products/{product_id}
     *
     * @param onLoadProductResponse - callback function on Success
     * @param onFail - callback function on Fail
     * @param orderId - orderId to load
     */
    fun loadProductById(
        onLoadProductResponse: (Product) -> Unit,
        onFail: () -> Unit,
        productId: Int
    ) {
        apiService?.getProductById(productId)?.enqueue(object : Callback<Product> {
            override fun onResponse(call: Call<Product>?, response: Response<Product>) {
                if (response.isSuccessful && response.body() != null) {
                    onLoadProductResponse(response.body()!!)
                } else {
                    onFail()
                    Log.e(TAG, "getProductById: error ${response.code()}")
                }
            }

            override fun onFailure(call: Call<Product>?, t: Throwable) {
                onFail()
                Log.e(TAG, "getProductById: error ${t.message}")
            }
        })
    }

    /**
     * Loads list of categories by the params from the api/2.0/categories
     *
     * @param onLoadCategoriesResponse - callback function
     * @param onFail       - callback function on Fail
     * @param params      - load params, to load information from API
     * @param isSearch - flag, true if it is Search form, false, if not
     */
    fun loadCategories(
        onLoadCategoriesResponse: (MutableList<Category>, LoadParameters, Boolean) -> Unit,
        onFail: () -> Unit,
        params: LoadParameters,
        isSearch: Boolean = false
    ) {
        val itemsPerPage = params.itemsPerPage
        val page = params.page
        val parentId = params.parentId
        val categoryIds = params.categoryIds
        apiService?.getCategories(itemsPerPage, page, parentId, categoryIds)?.enqueue(object : Callback<Categories.Response> {
            override fun onResponse(call: Call<Categories.Response>?, response: Response<Categories.Response>) {
                if (response.isSuccessful && response.body() != null) {
                    Log.e(TAG, "${response.body()!!.categoryList!!.count()}")
                    onLoadCategoriesResponse(
                        response.body()!!.categoryList!!,
                        response.body()!!.params!!,
                        isSearch
                    )
                } else {
                    onFail()
                    Log.e(TAG, "loadCategories: error ${response.code()}")
                }
            }

            override fun onFailure(call: Call<Categories.Response>?, t: Throwable) {
                onFail()
                Log.e(TAG, "loadCategories: error ${t.message}")
            }
        })
    }

    /**
     * Loads list of users from the api/2.0/users
     *
     * @param onLoadUsers - callback function
     * @param params      - load params, to load information from API
     */
    fun loadUsers(
        onLoadUsersResponse: (MutableList<User>, LoadParameters, Boolean) -> Unit,
        onFail: () -> Unit,
        params: LoadParameters,
        isSearch: Boolean
    )
    {
        val page = params.page
        val itemsPerPage = params.itemsPerPage
        val userType = params.userType!!
        val name = params.name
        apiService?.getUsers(page, itemsPerPage, userType, name)?.enqueue(object : Callback<Users.Response> {
            override fun onResponse(call: Call<Users.Response>?, response: Response<Users.Response>) {
                if (response.isSuccessful && response.body() != null) {
                    onLoadUsersResponse(
                        response.body()!!.usersList!!,
                        response.body()!!.params!!,
                        isSearch
                    )
                } else {
                    onFail()
                    Log.e(TAG, "error ${response.code()}")
                }
            }

            override fun onFailure(call: Call<Users.Response>?, t: Throwable) {
                onFail()
                Log.e(TAG, "error ${t.message}")
            }
        })
    }

    /**
     * Loads user by ID from the api/2.0/users/{id}
     *
     * @param onLoadUsers - callback function
     * @param params      - load params, to load information from API
     */
    fun loadUserById(
        onLoadUserResponse: (User) -> Unit,
        onFail: () -> Unit,
        userId: Int
    )
    {
        apiService?.getUserById(userId)?.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>?, response: Response<User>) {
                if (response.isSuccessful && response.body() != null) {
                    onLoadUserResponse(response.body()!!)
                } else {
                    onFail()
                    Log.e(TAG, "error ${response.code()}")
                }
            }

            override fun onFailure(call: Call<User>?, t: Throwable) {
                onFail()
                Log.e(TAG, "error ${t.message}")
            }
        })
    }

    /**
     * Loads list of settings from the api/2.0/settings
     *
     * @param onLoadSettings - callback function
     * @param params         - load params, to load information from API
     */
    fun loadSettings(
        onLoadSettings: (MutableMap<String, Setting>, LoadParameters) -> Unit,
        onFail: () -> Unit,
        params: LoadParameters
    ) {
        apiService?.getSettings(params.page, params.itemsPerPage)?.enqueue(object : Callback<Settings.Response> {
            override fun onFailure(call: Call<Settings.Response>, t: Throwable) {
                onFail()
                Log.e(TAG, "loadSettings: error ${t.message}")
            }

            override fun onResponse(
                call: Call<Settings.Response>,
                response: Response<Settings.Response>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    val settingsList = response.body()!!.settingsList!!
                    val settingsMap = mutableMapOf<String, Setting>()

                    settingsList.forEach {
                        settingsMap[it.name] = it
                    }

                    onLoadSettings(settingsMap, response.body()!!.params!!)
                } else {
                    onFail()
                    Log.e(TAG, "loadSettings: error ${response.code()}")
                }
            }
        })
    }

    /**
     * Loads storefront data from the api/4.0/sra
     *
     * @param onLoadCurrency - callback function
     */
    fun loadCurrency(onLoadCurrency: (Currency) -> Unit, onFail: () -> Unit)
    {
        apiService?.getCurrency()?.enqueue(object: Callback<Storefront> {
            override fun onResponse(call: Call<Storefront>, response: Response<Storefront>) {
                if (response.isSuccessful && response.body() != null) {
                    val currencyList = response.body()!!.currencies!!
                    var primaryCurrency = currencyList[0]

                    currencyList.forEach lit@{
                        if (it.isPrimaryBool()) {
                            primaryCurrency = it
                            return@lit
                        }
                    }

                    onLoadCurrency(primaryCurrency)
                } else {
                    onFail()

                    Log.e(TAG, "getCurrency: error ${response.code()}")
                }
            }

            override fun onFailure(call: Call<Storefront>, t: Throwable) {
                onFail()

                Log.e(TAG, "getCurrency: error ${t.message}")
            }
        })
    }

    /**
     * Update order
     *
     * @param onSuccess Callback function if order was updated successfully
     * @param onFail    Callback function if order was'not updated
     * @param order     An order which will be updated
     */
    fun updateOrder(
        onSuccess: (Order) -> Unit,
        onFail: () -> Unit,
        order: Order
    ) {
        var orderBody = Orders.Body()
        orderBody.status = order.status

        apiService!!.updateOrderById(order.getId(), orderBody).enqueue(object: Callback<Order> {
            override fun onResponse(call: Call<Order>, response: Response<Order>) {
                if (response.isSuccessful && response.body() != null) {
                    onSuccess(response.body()!!)
                } else {
                    onFail()
                    Log.e(TAG, "${response.code()} eerra")
                }
            }

            override fun onFailure(call: Call<Order>, t: Throwable) {
                onFail()
                Log.e(TAG, "${t.message} eerr")
            }
        })
    }

    /**
     * Update product
     *
     * @param onSuccess Callback function if order was updated successfully
     * @param onFail    Callback function if order was'not updated
     * @param product   Product which will be updated
     */
    fun updateProduct(
        onSuccess: (Product) -> Unit,
        onFail: () -> Unit,
        product: Products.Body
    ) {
        apiService!!.updateProductById(product.id, product).enqueue(object: Callback<Product> {
            override fun onResponse(call: Call<Product>, response: Response<Product>) {
                if (response.isSuccessful && response.body() != null) {
                    onSuccess(response.body()!!)
                } else {
                    onFail()
                    Log.e(TAG, "error ${response.message()}")
                }
            }

            override fun onFailure(call: Call<Product>, t: Throwable) {
                onFail()
                Log.e(TAG, "error ${t.message}")
            }
        })
    }

    /**
     * Create product
     *
     * @param onSuccess Callback function if order was updated successfully
     * @param onFail    Callback function if order was'not updated
     * @param product   Product which will be updated
     */
    fun createProduct(
        onSuccess: (Product) -> Unit,
        onFail: () -> Unit,
        product: Products.Body
    ) {
        apiService!!.createProduct(product).enqueue(object: Callback<Product> {
            override fun onResponse(call: Call<Product>, response: Response<Product>) {
                if (response.isSuccessful && response.body() != null) {
                    onSuccess(response.body()!!)
                } else {
                    onFail()
                    Log.e(TAG, "error ${response.message()}")
                }
            }

            override fun onFailure(call: Call<Product>, t: Throwable) {
                onFail()
                Log.e(TAG, "error ${t.message}")
            }
        })
    }

    /**
     * Update user
     *
     * @param onSuccess Callback function if order was updated successfully
     * @param onFail    Callback function if order was'not updated
     * @param order     Product which will be updated
     */
    fun updateUser(
        onSuccess: (User) -> Unit,
        onFail: () -> Unit,
        user: Users.Body
    ) {
        apiService!!.updateUserById(user.id, user).enqueue(object: Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (response.isSuccessful && response.body() != null) {
                    onSuccess(response.body()!!)
                } else {
                    onFail()
                }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                onFail()
            }
        })
    }

    /**
     * Login to the store via api/auth
     *
     * @param onSuccessLogin Callback function if login was success
     * @param onFailedLogin  Callback function if login was failed
     * @param login          Login email
     * @param password       Password
     * @param url            Store url
     */
    fun login(
        onSuccessLogin: (Login, Boolean) -> Unit,
        onFailedLogin: (Int) -> Unit,
        loginData: Login,
        isSaveLoginData: Boolean
    ) {
        val authBody = Auth.Body(loginData.login)

        apiService = ApiService.Factory.create(loginData) ?: return
        apiService!!.auth(authBody).enqueue(object : Callback<Auth.Response> {
            override fun onResponse(call: Call<Auth.Response>?, response: Response<Auth.Response>) {
                if (response.isSuccessful && response.body() != null) {
                    onSuccessLogin(loginData, isSaveLoginData)
                    Log.e(TAG, loginData.url)
                } else {
                    onFailedLogin(response.code())
                }
            }

            override fun onFailure(call: Call<Auth.Response>?, t: Throwable) {
                onFailedLogin(ParamIds.ERROR_FAILED_TO_LOAD)
            }
        })
    }

    /**
     * Gets store edition using /api/4.0/m_store_edition
     *
     * @param onSuccessLoad Callback function if load was success
     * @param onFailedLoad  Callback function if load was failed
     */
    fun getStoreEdition(
        onSuccessLoad: (String) -> Unit,
        onFailedLoad: () -> Unit
    ) {
        apiService!!.getStoreEdition().enqueue(object: Callback<StoreEdition.Response> {
            override fun onFailure(call: Call<StoreEdition.Response>, t: Throwable) {
                onFailedLoad()
            }

            override fun onResponse(
                call: Call<StoreEdition.Response>,
                response: Response<StoreEdition.Response>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    response.body()?.edition?.let { onSuccessLoad(it) }
                } else {
                    onFailedLoad()
                }
            }
        })
    }
}

