package com.asprojecteam.cs_cartadministrator.Tygh.Api.Entities

import com.google.gson.annotations.SerializedName

object Auth {

    class Response {
        @SerializedName("key")
        private var key: String = ""

        fun getSessionKey(): String {
            return key
        }
    }

    class Body (Email: String){

        @SerializedName("email")
        private var email: String = ""

        init {
            email = Email
        }
    }
}