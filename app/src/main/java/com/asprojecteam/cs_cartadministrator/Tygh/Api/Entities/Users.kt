package com.asprojecteam.cs_cartadministrator.Tygh.Api.Entities

import com.asprojecteam.cs_cartadministrator.Classes.User
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.google.gson.annotations.SerializedName

object Users {
    class Response {
        @SerializedName("users")
        var usersList: MutableList<User>? = null

        @SerializedName("params")
        var params: LoadParameters? = null
    }

    class Body {
        var id = 0

        @SerializedName("firstname")
        var firstName: String? = null

        @SerializedName("lastname")
        var lastName: String? = null

        @SerializedName("email")
        var email: String? = null

        @SerializedName("phone")
        var phone: String? = null

        @SerializedName("status")
        var status: String? = null
    }
}