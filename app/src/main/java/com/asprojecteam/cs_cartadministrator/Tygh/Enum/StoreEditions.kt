package com.asprojecteam.cs_cartadministrator.Tygh.Enum

object StoreEditions {
    const val MULTIVENDOR = 1
    const val CSCART = 2
    const val MULTIVENDOR_STR = "Multi-Vendor"
    const val CSCART_STR = "CS-Cart"
}