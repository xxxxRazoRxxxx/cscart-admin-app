package com.asprojecteam.cs_cartadministrator.Tygh.InterfaceBuilder

import android.content.Context
import android.graphics.Color
import android.text.TextUtils
import android.view.Gravity
import android.widget.*
import com.asprojecteam.cs_cartadministrator.R

const val LINEAR_LAYOUT = 0
const val TABLE_ROW_LAYOUT = 1

class TableRowBuilder (
    val context: Context
) {
    fun getRow(): Pair<TableRow, TableLayout.LayoutParams>
    {
        val tableRow = TableRow(context)
        val trParams = TableLayout.LayoutParams(
            TableLayout.LayoutParams.MATCH_PARENT,
            TableLayout.LayoutParams.MATCH_PARENT
        )
        tableRow.layoutParams = trParams

        return Pair(tableRow, trParams)
    }

    fun getRowSeparator(): Pair<TableRow, TableLayout.LayoutParams>
    {
        val rowSeparatorLayoutParams = TableLayout.LayoutParams(
            TableLayout.LayoutParams.MATCH_PARENT,
            TableLayout.LayoutParams.WRAP_CONTENT
        )
        rowSeparatorLayoutParams.setMargins(0, 0, 0, 0)

        val rowSeparator = TableRow(context)
        rowSeparator.layoutParams = rowSeparatorLayoutParams

        val tvSeparatorLayoutParams = TableRow.LayoutParams(
            TableRow.LayoutParams.MATCH_PARENT,
            TableRow.LayoutParams.WRAP_CONTENT
        )
        tvSeparatorLayoutParams.span = 4;

        val tvSeparator = TextView(context)
        tvSeparator.layoutParams = tvSeparatorLayoutParams
        tvSeparator.setBackgroundColor(Color.parseColor("#d9d9d9"));
        tvSeparator.height = 1

        rowSeparator.addView(tvSeparator)

        return Pair(rowSeparator, rowSeparatorLayoutParams)
    }

    fun getImageElement(sizeResourceId: Int): ImageView
    {
        val imageView = ImageView(context)
        val imageViewLayoutParams = LinearLayout.LayoutParams(sizeResourceId, sizeResourceId)
        imageViewLayoutParams.gravity = Gravity.CENTER
        imageView.layoutParams = imageViewLayoutParams

        return imageView
    }

    fun getMainTextElement(textSize: Float): TextView
    {
        val textView = TextView(context);
        val textViewLayout = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        textViewLayout.gravity = Gravity.CENTER_VERTICAL
        textViewLayout.weight = 2f
        textView.layoutParams = textViewLayout
        textView.ellipsize = TextUtils.TruncateAt.END
        textView.setLines(2)
        textView.textSize = textSize

        return textView
    }

    fun getTextElement(textSize: Float, gravity: Int, layout: Int, span: Int = 0): TextView
    {
        val textView = TextView(context)

        val textViewLayout = when (layout) {
            LINEAR_LAYOUT -> {
                 getLinearLayout()
            }
            TABLE_ROW_LAYOUT -> {
                getTableRowLayout()
            }
            else -> {
                getLinearLayout()
            }
        }

        if (textViewLayout is TableRow.LayoutParams && span != 0) {
            textViewLayout.span = span
        }

        textViewLayout.gravity = Gravity.CENTER_VERTICAL
        textViewLayout.weight = 0.5f
        textView.gravity = gravity
        textView.layoutParams = textViewLayout
        textView.textSize = textSize

        return textView
    }

    fun  getLinearLayout(marginLeft: Int, marginTop: Int, marginRight: Int, marginBottom: Int): LinearLayout
    {
        val linearLayout = LinearLayout(context)
        val llLayoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT)
        llLayoutParams.setMargins(marginLeft, marginTop, marginRight, marginBottom)
        linearLayout.orientation = LinearLayout.HORIZONTAL
        linearLayout.layoutParams = llLayoutParams
        return linearLayout
    }

    private fun getLinearLayout(): LinearLayout.LayoutParams
    {
        return LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
    }

    private fun getTableRowLayout(): TableRow.LayoutParams
    {
        return TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT)
    }
}