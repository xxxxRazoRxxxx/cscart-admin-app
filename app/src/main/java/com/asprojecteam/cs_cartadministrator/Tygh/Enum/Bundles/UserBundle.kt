package com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles

object UserBundle {
    const val USER_ID   = "user_id"
    const val USER_NAME = "user_name"
}