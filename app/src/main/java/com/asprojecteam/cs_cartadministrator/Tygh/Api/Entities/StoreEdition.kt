package com.asprojecteam.cs_cartadministrator.Tygh.Api.Entities

import com.google.gson.annotations.SerializedName

object StoreEdition {
    class Response {
        @SerializedName("edition")
        var edition: String? = null
    }
}