package com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.asprojecteam.cs_cartadministrator.Classes.Order
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentOrderDetailed
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentOrders
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles.OrderBundle
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.ParamIds

class OrdersAdapter(var items: MutableList<Order>, var params: LoadParameters, private val fragment: FragmentOrders) : RecyclerView.Adapter<OrdersAdapter.OrdersHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = OrdersHolder(LayoutInflater.from(parent.context).inflate(R.layout.fragment_orders_element, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: OrdersHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class OrdersHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val idText = itemView.findViewById<TextView>(R.id.order_id)
        private val nameText = itemView.findViewById<TextView>(R.id.order_customer)
        private val totalText = itemView.findViewById<TextView>(R.id.order_total)
        private val statusText = itemView.findViewById<TextView>(R.id.order_status)
        private val orderCardView = itemView.findViewById<CardView>(R.id.order_cardview)
        private val orderNextPageLoader = itemView.findViewById<ProgressBar>(R.id.order_nextpage_loader)

        fun bind(item: Order) {
            if (item.id == ParamIds.NEXT_PAGE_ID) {
                orderCardView.visibility = View.GONE
                orderNextPageLoader.visibility = View.VISIBLE

                fragment.mController.loadOrders(params.page + 1)
            } else {
                orderCardView.visibility = View.VISIBLE
                orderNextPageLoader.visibility = View.GONE

                idText.text = "${fragment.getString(R.string.order)} ${item.id}"
                nameText.text = item.getName()
                totalText.text = "${item.getTotalWithCurrency()} "
                statusText.text = "${item.statusParams.description}"

                statusText.setBackgroundColor(item.getStatusColor())
            }

            itemView.setOnClickListener {
                onItemClicked(item)
            }
        }
    }

    fun addLoadedOrders(newItems: List<Order>, newParams: LoadParameters)
    {
        params = newParams

        items.removeAt(items.size - 1)
        items.addAll(newItems)

        notifyItemRangeChanged(items.size - newItems.count(), newItems.count())
    }

    fun onItemClicked(item: Order){
        val bundle = Bundle()
        bundle.putInt(OrderBundle.ORDER_ID, item.getId())

        val fragmentOrderDetailed = FragmentOrderDetailed()
        val fragmentTrans = fragment.requireActivity().supportFragmentManager

        fragmentOrderDetailed.arguments = bundle

        fragmentTrans.beginTransaction()
            .replace(R.id.main_container, fragmentOrderDetailed)
            .addToBackStack(null)
            .commit()
    }
}