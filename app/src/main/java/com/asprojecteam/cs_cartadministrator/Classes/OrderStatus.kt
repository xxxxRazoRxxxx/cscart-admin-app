package com.asprojecteam.cs_cartadministrator.Classes

import com.google.gson.annotations.SerializedName

class OrderStatus
{
    @SerializedName("status_id")
    val id = 0

    @SerializedName("status")
    val status = ""

    @SerializedName("description")
    val description = ""

    @SerializedName("params")
    val params: StatusParams = StatusParams()
}

class StatusParams
{
    @SerializedName("color")
    val color = ""
}