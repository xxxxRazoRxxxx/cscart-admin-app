package com.asprojecteam.cs_cartadministrator.Classes

import com.asprojecteam.cs_cartadministrator.Tygh.Enum.ProductStatuses
import com.google.gson.annotations.SerializedName

class Product {
    @SerializedName("product_id")
    var id = 0

    @SerializedName("product_code")
    var code = ""

    @SerializedName("product")
    var product = ""

    @SerializedName("price")
    var price: Double = 0.0

    @SerializedName("status")
    var status = ProductStatuses.ACTIVE

    @SerializedName("amount")
    var amount = 0

    @SerializedName("subtotal")
    val subtotal = ""

    @SerializedName("main_pair")
    var mainPair: ImagePair? = null

    @SerializedName("image_pairs")
    var imagePairs: HashMap<String, ImagePair> = hashMapOf()

    @SerializedName("category_ids")
    var categoryIds: MutableList<Int> = mutableListOf()

    var categoryList: MutableList<Category> = mutableListOf()

    var currency = Currency()

    fun getPriceWithCurrency(): String
    {
        return currency.getStringWithCurrencySymbol(this.price.toString())
    }
}

class ImagePair {
    @SerializedName("detailed_id")
    var detailedId: Int = 0

    @SerializedName("image_id")
    var imageId: Int = 0

    @SerializedName("pair_id")
    var pairId: Int = 0

    @SerializedName("icon")
    var icon: Image = Image()

    @SerializedName("detailed")
    var detailed: Image = Image()

    @SerializedName("absolute_path")
    var absolutePath: String? = null

    @SerializedName("alt")
    var alt: String? = null

    @SerializedName("http_image_path")
    var httpImagePath: String? = null

    @SerializedName("image_path")
    var imagePath: String? = null

    @SerializedName("position")
    var position: Int = 0

    @SerializedName("image_x")
    var imageX: Int = 0

    @SerializedName("image_y")
    var imageY: Int = 0
}

class Image {
    @SerializedName("image_path")
    val image_path = ""

    @SerializedName("absolute_path")
    var absolutePath: String? = null

    @SerializedName("relative_path")
    var relativePath: String? = null
}

class ProductsGroups {
    @SerializedName("products")
    val products: HashMap<String, Product>? = null
}