package com.asprojecteam.cs_cartadministrator.Classes

import android.text.Html
import android.text.Spanned
import androidx.core.text.HtmlCompat
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.YesNo
import com.google.gson.annotations.SerializedName
import java.util.*

class Currency {
    @SerializedName("currency_code")
    val currencyCode = ""

    @SerializedName("symbol")
    val symbol = ""

    @SerializedName("coefficient")
    val coefficient = ""

    @SerializedName("is_primary")
    val isPrimary = ""

    /**
     * Checks if currency is primary
     */
    fun isPrimaryBool(): Boolean
    {
        return isPrimary == YesNo.YES
    }

    /**
     * Gets amount with currency symbol
     *
     * @param string
     */
    fun getStringWithCurrencySymbol(string: String): String
    {
        var amountString = string
        if (!isPrimaryBool()) {
            val amount = string.toFloat() / getCoefficient()
            amountString = String.format(Locale.US, "%.2f", amount)
        }

        return "$amountString ${getSymbol()}"
    }

    /**
     * Gets coefficient of the currency
     */
    private fun getCoefficient(): Float
    {
        return if (this.coefficient.isBlank()) {
            1f
        } else {
            this.coefficient.toFloat()
        }
    }

    fun getSymbol(): Spanned? {
        val string = when(currencyCode) {
            "USD" -> "&#36;"
            "RUB" -> "&#8381;"
            "EUR" -> "&#8364;"
            "GBP" -> "&#163;"
            else -> ""
        }

        return HtmlCompat.fromHtml(string, HtmlCompat.FROM_HTML_MODE_LEGACY);
    }
}