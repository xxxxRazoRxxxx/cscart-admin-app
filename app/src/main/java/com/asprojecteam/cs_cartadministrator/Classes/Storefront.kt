package com.asprojecteam.cs_cartadministrator.Classes

import com.google.gson.annotations.SerializedName

class Storefront {
    @SerializedName("currencies")
    val currencies: List<Currency>? = null
}