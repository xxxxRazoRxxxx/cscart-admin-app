package com.asprojecteam.cs_cartadministrator.Classes

import com.google.gson.annotations.SerializedName

class PaymentMethod {
    @SerializedName("payment")
    val payment = ""

    @SerializedName("description")
    val description = ""

    override fun toString(): String {
        return "$payment ($description)"
    }
}

class PaymentInfo: Order() {
    @SerializedName("customer_phone")
    val customerPhone: String? = null
}