package com.asprojecteam.cs_cartadministrator.Classes

import android.graphics.Color
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.HashMap

open class Order
{
    @SerializedName("order_id")
    var id: Int? = null

    @SerializedName("total")
    val total: Double = 0.0

    @SerializedName("firstname")
    val firstname = ""

    @SerializedName("s_firstname")
    val shippingFirstname = ""

    @SerializedName("b_firstname")
    val billingFirstname = ""

    @SerializedName("lastname")
    val lastname = ""

    @SerializedName("s_lastname")
    val shippingLastname = ""

    @SerializedName("b_lastname")
    val billingLastname = ""

    @SerializedName("email")
    val email = ""

    @SerializedName("ip_address")
    val ipAddress = ""

    @SerializedName("phone")
    val phone = ""

    @SerializedName("status")
    var status = ""

    @SerializedName("subtotal")
    val subtotal = ""

    @SerializedName("shipping_cost")
    val shippingCost = ""

    @SerializedName("s_address")
    val shippingAddress = ""

    @SerializedName("s_address2")
    val shippingAddress2 = ""

    @SerializedName("s_city")
    val shippingCity = ""

    @SerializedName("s_state")
    val shippingState = ""

    @SerializedName("s_state_descr")
    val shippingStateDescription = ""

    @SerializedName("s_country_descr")
    val shippingCountry = ""

    @SerializedName("s_zipcode")
    val shippingZipcode = ""

    @SerializedName("s_phone")
    val shippingPhone = ""

    @SerializedName("b_address")
    val billingAddress = ""

    @SerializedName("b_address2")
    val billingAddress2 = ""

    @SerializedName("b_city")
    val billingCity = ""

    @SerializedName("b_state")
    val billingState = ""

    @SerializedName("b_state_descr")
    val billingStateDescription = ""

    @SerializedName("b_country_descr")
    val billingCountry = ""

    @SerializedName("b_zipcode")
    val billingZipcode = ""

    @SerializedName("b_phone")
    val billingPhone = ""

    @SerializedName("products")
    val products: HashMap<String, Product>? = null

    @SerializedName("product_groups")
    val productGroups: List<ProductsGroups>? = null

    @SerializedName("taxes")
    val taxes: HashMap<String, Tax>? = null

    @SerializedName("payment_method")
    val paymentMethod = PaymentMethod()

    @SerializedName("payment_info")
    val paymentInfo: PaymentInfo? = null

    @SerializedName("shipping")
    val shipping: List<Shipping>? = null

    var statusParams = OrderStatus()

    var currency = Currency()

    fun getName(): String
    {
        return "$firstname $lastname"
    }

    fun getShippingName(): String
    {
        return "$shippingFirstname $shippingLastname"
    }

    fun getShippingStateString(): String
    {
        return "$shippingStateDescription ($shippingState)"
    }

    fun getBillingName(): String
    {
        return "$billingFirstname $billingLastname"
    }

    fun getBillingStateString(): String
    {
        return "$billingStateDescription ($billingState)"
    }

    fun getId(): Int
    {
        return this.id!!;
    }

    fun getTotalWithCurrency(): String
    {
        return currency.getStringWithCurrencySymbol(this.total.toString())
    }

    fun getSubtotalWithCurrency(): String
    {
        return currency.getStringWithCurrencySymbol(this.subtotal)
    }

    fun getShippingCostWithCurrency(): String
    {
        return currency.getStringWithCurrencySymbol(this.shippingCost)
    }

    fun getStatusColor(): Int
    {
        val statusColor = statusParams.params.color
        return Color.parseColor(statusColor.toUpperCase(Locale.ROOT))
    }
}