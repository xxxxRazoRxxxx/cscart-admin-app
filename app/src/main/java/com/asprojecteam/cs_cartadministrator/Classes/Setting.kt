package com.asprojecteam.cs_cartadministrator.Classes

import android.content.Context
import com.asprojecteam.cs_cartadministrator.R
import com.google.gson.annotations.SerializedName

class Setting {
    @SerializedName("object_id")
    var objectId = ""

    @SerializedName("name")
    var name = ""

    @SerializedName("value")
    var value = ""
}