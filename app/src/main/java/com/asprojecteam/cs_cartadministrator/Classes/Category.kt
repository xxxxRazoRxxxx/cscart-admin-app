package com.asprojecteam.cs_cartadministrator.Classes

import com.google.gson.annotations.SerializedName

class Category {
    var isChecked = false

    @SerializedName("category")
    var category = ""

    @SerializedName("category_id")
    var id = 0

    @SerializedName("parent_id")
    val parentId = 0

    @SerializedName("product_count")
    val productCount = 0
}