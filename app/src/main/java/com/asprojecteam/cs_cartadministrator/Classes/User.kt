package com.asprojecteam.cs_cartadministrator.Classes

import com.google.gson.annotations.SerializedName

class User {
    @SerializedName("user_id")
    var id = 0

    @SerializedName("email")
    var email = ""

    @SerializedName("firstname")
    var firstname = ""

    @SerializedName("lastname")
    var lastname = ""

    @SerializedName("phone")
    var phone = ""

    @SerializedName("status")
    var status = ""

    fun getName(): String
    {
        return "$firstname $lastname"
    }
}