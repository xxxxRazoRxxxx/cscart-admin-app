package com.asprojecteam.cs_cartadministrator.Classes

import android.content.Context
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.YesNo
import com.google.gson.annotations.SerializedName

class Tax {
    @SerializedName("description")
    val description = ""

    @SerializedName("rate_value")
    val rateValue = ""

    @SerializedName("price_includes_tax")
    val priceIncludesTax = ""

    @SerializedName("regnumber")
    val regNumber = ""

    @SerializedName("tax_subtotal")
    val taxSubtotal = 0f

    fun getTaxString(context: Context): String
    {
        var included = ""
        if (isPriceIncludesTax()) {
            included = context.resources.getString(R.string.taxes_included)
            included = "$included "
        }

        val rateValue = if(this.rateValue.toFloat().rem(1) == 0f) {
            this.rateValue.toFloat().toInt().toString()
        } else {
            this.rateValue.toFloat().toString()
        }

        return "$description $rateValue% $included ($regNumber)"
    }

    private fun isPriceIncludesTax(): Boolean
    {
        return priceIncludesTax === YesNo.YES
    }
}