package com.asprojecteam.cs_cartadministrator.Classes

/**
 * Login class
 *
 * @param login    Login to login
 * @param password Password to login
 * @param url      Url of the store
 */
class Login(
    val login: String,
    val password: String,
    val url: String
)
