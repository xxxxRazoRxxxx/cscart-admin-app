package com.asprojecteam.cs_cartadministrator.Classes

import com.google.gson.annotations.SerializedName

class Shipping {

    @SerializedName("shipping")
    val shipping = ""

    @SerializedName("group_name")
    val groupName: String? = null
}