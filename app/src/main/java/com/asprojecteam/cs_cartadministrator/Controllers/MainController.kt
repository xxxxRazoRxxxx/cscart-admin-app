package com.asprojecteam.cs_cartadministrator.Controllers

import android.content.ContentValues.TAG
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import com.asprojecteam.cs_cartadministrator.Fragments.*
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.MainActivity
import com.asprojecteam.cs_cartadministrator.Tygh.Data.Saver
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Saver.Types
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.StoreEditions
import com.asprojecteam.cs_cartadministrator.Tygh.Inrefaces.OnBackPressedListener
import com.google.android.material.navigation.NavigationView


/**
 * Controller class of the MainActivity
 *
 * @param mView
 */
class MainController (
    val mView: MainActivity
) {
    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    fun onBackPressed(): Boolean
    {
        var backPressedListener: OnBackPressedListener? = null
        val fragmentManager = mView.supportFragmentManager
        fragmentManager.fragments.forEach lit@{
            if (it is FragmentLogin) {
                return false
            }

            if (it is OnBackPressedListener) {
                backPressedListener = it
                return@lit
            }
        }

        var backPressed: Boolean = false

        if (backPressedListener != null) {
            backPressed = backPressedListener!!.onBackPressed()
        }

        if (!backPressed) {
            val fragmentDashboard = FragmentDashboard()

            if (mView.currentFragment == R.id.nav_home_menu) {
                return false
            }

            val fragmentTrans = mView.supportFragmentManager.beginTransaction()
            fragmentTrans.replace(R.id.main_container, fragmentDashboard)
            fragmentTrans.commit()

            mView.currentFragment = R.id.nav_home_menu
            mView.bottomNavigationView.selectedItemId = R.id.nav_home_menu

            return true
        }

        return backPressed
    }

    /**
     * Callback function on bottom menu item was clicked
     *     - Implements action which was chosen in the menu
     *
     * @return NavigationView.OnNavigationItemSelectedListener
     */
    fun onBottomMenuSelected(itemId: Int): Boolean {
        var selectedFragment: Fragment? = null

        when (itemId) {
            R.id.nav_home_menu -> {
                selectedFragment = FragmentDashboard()
                if (mView.drawerLayout.isDrawerOpen(mView.leftSideBarMenu)) {
                    mView.drawerLayout.closeDrawer(mView.leftSideBarMenu)
                }
            }
            R.id.nav_orders_menu -> {
                selectedFragment = FragmentOrders()
                if (mView.drawerLayout.isDrawerOpen(mView.leftSideBarMenu)) {
                    mView.drawerLayout.closeDrawer(mView.leftSideBarMenu)
                }
            }
            R.id.nav_products_menu -> {
                selectedFragment = FragmentProducts()
                if (mView.drawerLayout.isDrawerOpen(mView.leftSideBarMenu)) {
                    mView.drawerLayout.closeDrawer(mView.leftSideBarMenu)
                }
            }
            R.id.nav_users_menu -> {
                selectedFragment = FragmentUsers()
                if (mView.drawerLayout.isDrawerOpen(mView.leftSideBarMenu)) {
                    mView.drawerLayout.closeDrawer(mView.leftSideBarMenu)
                }
            }
            R.id.nav_sidebar_menu -> {
                when(mView.drawerLayout.isDrawerOpen(mView.leftSideBarMenu)) {
                    true -> {
                        mView.drawerLayout.closeDrawer(mView.leftSideBarMenu)
                        mView.bottomNavigationView.selectedItemId = mView.currentFragment
                    }
                    false -> mView.drawerLayout.openDrawer(mView.leftSideBarMenu)
                }
            }
        }

        val fragmentTrans = mView.supportFragmentManager.beginTransaction()

        if(selectedFragment != null && itemId != mView.currentFragment) {
            if (itemId != R.id.nav_sidebar_menu) {
                mView.currentFragment = itemId
            }

            fragmentTrans.replace(R.id.main_container, selectedFragment)
            fragmentTrans.commit()
        }

        return true
    }

    /**
     * Callback function on Side Bar menu Item clicked:
     *     - Implements action which was chosen in the menu
     *
     * @return NavigationView.OnNavigationItemSelectedListener
     */
    fun onSideBarMenuItemSelected(): NavigationView.OnNavigationItemSelectedListener
    {

        return NavigationView.OnNavigationItemSelectedListener {
            var selectedFragment: Fragment? = null

            when(it.itemId) {
                R.id.side_bar_menu_categories -> {
                    selectedFragment = FragmentCategories()
                    if (mView.drawerLayout.isDrawerOpen(mView.leftSideBarMenu)) {
                        mView.drawerLayout.closeDrawer(mView.leftSideBarMenu)
                    }

                    val fragmentTrans = mView.supportFragmentManager.beginTransaction()

                    fragmentTrans.replace(R.id.main_container, selectedFragment)
                    fragmentTrans.commit()
                    true
                }
                R.id.side_bar_menu_disconnect -> {
                    disconnect()
                    mView.mPresenter.closeSideBarMenu()
                    true
                }
                R.id.side_bar_menu_exit -> {
                    exitApp()
                    mView.mPresenter.closeSideBarMenu()
                    true
                }
                else -> false
            }
        }
    }

    /**
     * Changes store edition
     */
    fun changeStoreEdition(edition: Int)
    {
        mView.storeEdition = edition

        if (edition == StoreEditions.MULTIVENDOR) {
            mView.storeEditionTextView.text = mView.resources.getText(R.string.store_edition_multivendor)
        } else if (edition == StoreEditions.CSCART) {
            mView.storeEditionTextView.text = mView.resources.getText(R.string.store_edition_cscart)
        }
    }

    /**
     * Changes store edition
     */
    fun changeAddonStatus(status: Boolean)
    {
        if (status) {
            mView.addonInstalled = true
            mView.addonStatusTextView.visibility = View.GONE
        } else {
            mView.addonInstalled = false
            mView.addonStatusTextView.visibility = View.VISIBLE
        }
    }

    /**
     * Disconnect from the shop and opens login page
     */
    private fun disconnect()
    {
        val saver = Saver(Types.SETTINGS_LOGIN, mView.applicationContext)
        saver.saveBoolean(Types.LOGIN_SUCCESSFUL, false)
        mView.mPresenter.openLoginPage()
    }

    /**
     * Exit the app
     */
    private fun exitApp()
    {
        mView.finishAffinity();
    }
}
