package com.asprojecteam.cs_cartadministrator.Controllers

import android.view.MenuItem
import android.view.View
import android.widget.RadioButton
import com.asprojecteam.cs_cartadministrator.Classes.User
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentUserDetailed
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.Entities.Products
import com.asprojecteam.cs_cartadministrator.Tygh.Api.Entities.Users
import com.asprojecteam.cs_cartadministrator.Tygh.DataLoader
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.UserStatuses

/**
 * Controller class of the FragmentUserDetailed
 *
 * @param mView FragmentUserDetailed
 */
class UserDetailedController (
    val mView: FragmentUserDetailed
) {
    /** API data loader */
    private var dataLoader: DataLoader = DataLoader(mView.requireContext())

    /**
     * Updates user if SAVE button was clicked
     */
    fun onSaveMenuItemClicked(): MenuItem.OnMenuItemClickListener
    {
        return MenuItem.OnMenuItemClickListener {

            val userBody = Users.Body()
            userBody.id = mView.user.id

            if (mView.userFirstNameEditText.text.toString() != mView.user.firstname) {
                userBody.firstName = mView.userFirstNameEditText.text.toString()
            }

            if (mView.userLastNameEditText.text.toString() != mView.user.lastname) {
                userBody.lastName = mView.userLastNameEditText.text.toString()
            }

            if (mView.userEmailEditText.text.toString() != mView.user.email) {
                userBody.email = mView.userEmailEditText.text.toString()
            }

            if (mView.userPhoneEditText.text.toString() != mView.user.phone) {
                userBody.phone = mView.userPhoneEditText.text.toString()
            }

            userBody.status = mView.user.status

            dataLoader.updateUser(
                ::onUserUpdatedSuccess,
                ::onUserUpdatedFail,
                userBody
            )

            true
        }
    }

    /**
     * Changes user status, if status radio button was changed
     */
    fun onStatusClicked(): View.OnClickListener
    {
        return View.OnClickListener {
            val radioButton = (it as RadioButton)
            when (radioButton.id) {
                R.id.radio_user_status_active   -> mView.user.status = UserStatuses.ACTIVE
                R.id.radio_user_status_disabled -> mView.user.status = UserStatuses.DISABLED
            }
        }
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    fun onBackPressed(): Boolean
    {
        mView.requireActivity().supportFragmentManager.popBackStack()

        return true
    }

    /**
     * Actions after user was successfully updated
     *
     * @param product Product which was updated
     */
    private fun onUserUpdatedSuccess(user: User)
    {
        val message = mView.resources.getString(R.string.user_was_updated)
        mView.interfaceManager.showMessage(message)
    }

    /**
     * Actions after user update was failed
     */
    private fun onUserUpdatedFail()
    {
        val message = mView.resources.getString(R.string.user_was_not_updated)
        mView.interfaceManager.showMessage(message)
    }
}