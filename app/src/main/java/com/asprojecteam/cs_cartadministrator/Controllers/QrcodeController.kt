package com.asprojecteam.cs_cartadministrator.Controllers

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.core.app.ActivityCompat
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentQrcode
import com.asprojecteam.cs_cartadministrator.Presenters.QrcodePresenter
import com.asprojecteam.cs_cartadministrator.Tygh.Common
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Login.Results
import com.google.zxing.Result

const val REQUEST_CAMERA = 1

/**
 * Controller class of the FragmentQrcode
 *
 * @param mView FragmentQrcode
 */
class QrcodeController (
    val mView: FragmentQrcode
) {
    /**
     * Handles QR code scan result and put result to Bundle
     *
     * @param rawResult
     */
    fun handleQrScanResult(rawResult: Result?)
    {
        Common.vibratePhone(100, mView.requireContext())

        val bundle = Bundle()

        if(rawResult != null && rawResult.text != null) {
            bundle.putString(Results.RESULT, rawResult.text)
        }

        mView.mPresenter.openLoginPage(bundle)
    }

    /**
     * Actions on request permissions result
     *
     * @param requestCode
     * @param grantResults
     */
    fun onRequestPermissionsResult(
        requestCode: Int ,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CAMERA -> {
                if (grantResults.isNotEmpty()) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        mView.mScannerView.setResultHandler(mView)
                        mView.mScannerView.startCamera()
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(mView.requireActivity(),
                                arrayOf(Manifest.permission.CAMERA),
                                REQUEST_CAMERA)
                        }
                    }
                }
            }
        }
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    fun onBackPressed(): Boolean
    {
        mView.requireActivity().supportFragmentManager.popBackStackImmediate()

        return true
    }
}