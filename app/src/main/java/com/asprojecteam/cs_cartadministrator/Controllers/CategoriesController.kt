package com.asprojecteam.cs_cartadministrator.Controllers

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.asprojecteam.cs_cartadministrator.Classes.Category
import com.asprojecteam.cs_cartadministrator.Classes.Product
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentCategories
import com.asprojecteam.cs_cartadministrator.MainActivity
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.DataLoader
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles.ProductBundle
import com.asprojecteam.cs_cartadministrator.Tygh.ITEMS_PER_PAGE
import com.miguelcatalan.materialsearchview.MaterialSearchView

class CategoriesController (
    val mView: FragmentCategories
) {
    /**
     * API loader
     */
    private var dataLoader: DataLoader = DataLoader(mView.requireContext())

    /**
     * Loads necessary data for this page
     */
    fun loadData()
    {
        mView.interfaceManager.showLoader()
        val params = LoadParameters(1, ITEMS_PER_PAGE)
        loadCategories(1, params)
    }

    /**
     * Load categories
     *
     * @param page       Page of the categories
     * @param loadParams Params to load categories
     * @param isSearch   Flag if it is search load
     */
    fun loadCategories(page: Int = 1, loadParams: LoadParameters? = null, isSearch: Boolean = false)
    {
        val params: LoadParameters?
        params = loadParams ?: LoadParameters(page, ITEMS_PER_PAGE)
        params.page = page

        val onFailLoad = fun () {
            mView.interfaceManager.hideLoader()
            mView.interfaceManager.showNoDataError()
        }

        dataLoader.loadCategories(::onLoadCategories, onFailLoad, params, isSearch)
    }

    /**
     * Set product id and open product detailed page
     *
     * @param item Product data
     */
    fun onCategoryClicked(item: Category, cardView: CardView){
        val activity = (mView.requireActivity() as MainActivity)

        if (mView.selectableCategories) {
            if (!item.isChecked) {
                item.isChecked = true

                cardView.setCardBackgroundColor(ContextCompat.getColor(mView.requireContext(), R.color.cardSelectedBackground))

                activity.selectedCategories[item.id] = item.category

                if (activity.selectedCategories.count() > 0) {
                    mView.checkButton.isVisible = true
                }
            } else {
                item.isChecked = false

                cardView.setCardBackgroundColor(ContextCompat.getColor(mView.requireContext(), R.color.cardWhiteBackground))

                activity.selectedCategories.remove(item.id)

                if (activity.selectedCategories.count() == 0) {
                    mView.checkButton.isVisible = false
                }
            }
        } else {
            //TODO: Open Category detailed page
        }
    }

    /**
     * Open back stack fragment if Check button clicked.
     */
    fun onCheckCategoriesMenuItemClicked(): MenuItem.OnMenuItemClickListener {
        return MenuItem.OnMenuItemClickListener {
            if (mView.selectableCategories) {
                mView.requireActivity().supportFragmentManager.popBackStack()
            }

            true
        }
    }

    /**
     * Actions after categories was loaded
     *
     * @param categoriesList Categories list
     * @param params         Params of the loaded products
     * @param isSearch       Flag if it search load
     */
    private fun onLoadCategories(categoriesList: MutableList<Category>, params: LoadParameters, isSearch: Boolean)
    {
        if (categoriesList.count() == 0) {
            mView.interfaceManager.showNoDataError()
        } else {
            mView.mPresenter.showCategories(categoriesList, params, isSearch)
        }
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    fun onBackPressed(): Boolean
    {
        if (mView.selectableCategories) {
            mView.requireActivity().supportFragmentManager.popBackStack()
            return true
        } else {
            if (mView.interfaceManager.onBackPressed()) {
                return true
            }
        }

        return false
    }

    /**
     * Try to search product if search query text changed
     */
    fun onSearchQueryTextListener(): MaterialSearchView.OnQueryTextListener
    {
        return object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean { //Do some magic
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean { //Do some magic
                mView.searchText = newText!!

                /*if (newText != "") {
                    mView.interfaceManager.showLoader()

                    val params = LoadParameters(1, ITEMS_PER_PAGE)
                    params.query = newText
                    dataLoader.loadProducts(
                        ::onLoadProducts,
                        mView.interfaceManager::showNoDataError,
                        params,
                        true
                    )
                } else {
                    mView.mPresenter.showDefaultList()
                }*/
                return false
            }
        }
    }

    /**
     * Shows default product list if the search view was closed
     */
    fun onSearchViewListener(): MaterialSearchView.SearchViewListener
    {
        return object: MaterialSearchView.SearchViewListener {
            override fun onSearchViewClosed() {
                //mView.mPresenter.showDefaultList()
            }

            override fun onSearchViewShown() {

            }
        }
    }
}