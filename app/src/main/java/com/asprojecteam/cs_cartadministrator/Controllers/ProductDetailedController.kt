package com.asprojecteam.cs_cartadministrator.Controllers

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.RadioButton
import android.widget.RelativeLayout
import com.asprojecteam.cs_cartadministrator.Classes.Category
import com.asprojecteam.cs_cartadministrator.Classes.Currency
import com.asprojecteam.cs_cartadministrator.Classes.Product
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentCategories
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentProductDetailed
import com.asprojecteam.cs_cartadministrator.MainActivity
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.Entities.Products
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.DataLoader
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles.GeneralBundle
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.ProductStatuses

/**
 * Controller class of the FragmentProductDetailed
 *
 * @param mView FragmentProductDetailed
 */
class ProductDetailedController (
    val mView: FragmentProductDetailed
) {
    /** API data loader */
    private var dataLoader: DataLoader = DataLoader(mView.requireContext())

    /**
     * Initializes controller and load necessary information
     */
    fun loadProduct()
    {
        mView.interfaceManager.showLoader()

        dataLoader.loadCurrency(::onCurrencyLoaded, mView.mPresenter::onProductLoadFailed)
    }

    /**
     * Actions after currency was loaded successfully
     *
     * @param primaryCurrency Currency to show data with it
     */
    private fun onCurrencyLoaded(primaryCurrency: Currency)
    {
        mView.currency = primaryCurrency

        if (mView.productId == 0) {
            mView.product = Product()
            onProductLoaded(mView.product)
        } else {
            dataLoader.loadProductById(
                ::onProductLoaded,
                mView.mPresenter::onProductLoadFailed,
                mView.productId!!
            )
        }
    }

    /**
     * Saves entered product name to product
     *
     * @return View.OnFocusChangeListener
     */
    fun onProductNameEditTextFocusChanged(): View.OnFocusChangeListener
    {
        return View.OnFocusChangeListener { _: View, hasFocus: Boolean ->
            if (!hasFocus) {
                mView.product.product = mView.productNameEditText.text.toString()
            }
        }
    }

    /**
     * Saves entered product code to product
     *
     * @return View.OnFocusChangeListener
     */
    fun onProductCodeEditTextFocusChanged(): View.OnFocusChangeListener
    {
        return View.OnFocusChangeListener { _: View, hasFocus: Boolean ->
            if (!hasFocus) {
                mView.product.code = mView.productCodeEditText.text.toString()
            }
        }
    }

    /**
     * Saves entered product price to product
     *
     * @return View.OnFocusChangeListener
     */
    fun onProductPriceEditTextFocusChanged(): View.OnFocusChangeListener
    {
        return View.OnFocusChangeListener { _: View, hasFocus: Boolean ->
            if (!hasFocus) {
                mView.product.price = mView.productPriceEditText.text.toString().toDouble()
            }
        }
    }

    /**
     * Saves entered product amount to product
     *
     * @return View.OnFocusChangeListener
     */
    fun onProductAmountEditTextFocusChanged(): View.OnFocusChangeListener
    {
        return View.OnFocusChangeListener { _: View, hasFocus: Boolean ->
            if (!hasFocus) {
                mView.product.amount = mView.productAmountEditText.text.toString().toInt()
            }
        }
    }

    /**
     * Actions after product data was loaded successfully
     *
     * @param loadedProduct Loaded data of the order
     */
    private fun onProductLoaded(loadedProduct: Product)
    {
        mView.product = loadedProduct

        val params = LoadParameters()
        params.categoryIds = loadedProduct.categoryIds

        if (mView.product.id != 0) {
            dataLoader.loadCategories(
                ::onCategoriesLoaded,
                mView.mPresenter::onProductLoadFailed,
                params,
                false
            )
        } else {
            mView.mPresenter.showProductInfo(mView.product)
        }
    }

    /**
     * Actions after category list data was loaded successfully
     *
     * @param categoryList List of categories
     * @param params       Params of loaded categories
     * @param isSearch     Flag if categories was loaded with search query
     */
    private fun onCategoriesLoaded(categoryList: MutableList<Category>, params: LoadParameters, isSearch: Boolean) {
        mView.product.categoryList = categoryList

        mView.mPresenter.showProductInfo(mView.product)
    }

    /**
     *
     */
    fun onRemoveCategoryButtonClicked(item: RelativeLayout, id: Int)
    {
        mView.productCategoryList.removeView(item)
        mView.product.categoryIds.remove(id)
    }

    /**
     *
     */
    fun onCategorySelected(id: Int, CategoryName: String)
    {
        mView.product.categoryIds.add(id)
    }

    /**
     * Updates product if SAVE button was clicked
     */
    fun onSaveMenuItemClicked(): MenuItem.OnMenuItemClickListener
    {
        return MenuItem.OnMenuItemClickListener {

            val productBody = Products.Body()
            productBody.id = mView.product.id

            if (mView.productNameEditText.text.isBlank()) {
                mView.interfaceManager.showSnackbarMessage(mView.resources.getString(R.string.product_product_name_is_mandatory))
                return@OnMenuItemClickListener true
            }

            if (mView.productPriceEditText.text.isBlank()) {
                mView.interfaceManager.showSnackbarMessage(mView.resources.getString(R.string.product_product_price_is_mandatory))
                return@OnMenuItemClickListener true
            }

            if (mView.product.categoryIds.count() == 0) {
                mView.interfaceManager.showSnackbarMessage(mView.resources.getString(R.string.product_categories_is_mandatory))
                return@OnMenuItemClickListener true
            }

            productBody.name = mView.productNameEditText.text.toString()
            productBody.productCode = mView.productCodeEditText.text.toString()
            productBody.price = mView.productPriceEditText.text.toString().toDouble()
            productBody.amount = mView.productAmountEditText.text.toString().toInt()

            productBody.categoryIds = mView.product.categoryIds
            productBody.status = mView.product.status

            if (mView.product.id == 0) {
                dataLoader.createProduct(
                    ::onProductUpdatedSuccess,
                    ::onProductUpdatedFail,
                    productBody
                )
            } else {
                dataLoader.updateProduct(
                    ::onProductUpdatedSuccess,
                    ::onProductUpdatedFail,
                    productBody
                )
            }

            true
        }
    }

    /**
     * Shows message about install addon from market place
     */
    fun onAddImageButtonClicked(): View.OnClickListener
    {
        return View.OnClickListener {
            val message = mView.resources.getString(R.string.install_addon_from_marketplace)
            mView.interfaceManager.showMessage(message)
        }
    }

    /**
     * Opens FragmentCategories to choose categories
     */
    fun onAddCategoryButtonClicked(): View.OnClickListener
    {
        return View.OnClickListener {
            val bundle = Bundle()
            bundle.putBoolean(GeneralBundle.ENABLE_SELECT, true)

            val fragmentCategories = FragmentCategories()
            fragmentCategories.arguments = bundle

            mView.requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.main_container, fragmentCategories)
                .addToBackStack(null)
                .commit()
        }
    }

    /**
     * TODO: Actions:
     * - show menu with variants: a)open image b)delete image
     */
    fun onImageLongClicked(): View.OnLongClickListener
    {
        return View.OnLongClickListener {
            true
        }
    }

    /**
     * Changes product status, if status radio button was changed
     */
    fun onStatusClicked(): View.OnClickListener
    {
        return View.OnClickListener {
            val radioButton = (it as RadioButton)
            when (radioButton.id) {
                R.id.radio_product_status_active   -> mView.product.status = ProductStatuses.ACTIVE
                R.id.radio_product_status_hidden   -> mView.product.status = ProductStatuses.HIDDEN
                R.id.radio_product_status_disabled -> mView.product.status = ProductStatuses.DISABLED
            }
        }
    }

    /* Actions on Back button pressed
     *
     * @return Boolean
     */
    fun onBackPressed(): Boolean
    {
        mView.requireActivity().supportFragmentManager.popBackStack()
        (mView.requireActivity()  as MainActivity).selectedCategories.clear()
        (mView.requireActivity()  as MainActivity).changedProduct = mView.product

        return true
    }

    /**
     * Actions after product was successfully updated
     *
     * @param product Product which was updated
     */
    private fun onProductUpdatedSuccess(product: Product)
    {
        if (mView.productId == 0) {
            mView.productId = product.id

            val message = mView.resources.getString(R.string.product_was_created)
        } else {
            val message = mView.resources.getString(R.string.product_was_updated)
        }

        loadProduct()
    }

    /**
     * Actions after product update was failed
     */
    private fun onProductUpdatedFail()
    {
        val message = mView.resources.getString(R.string.product_was_not_updated)
        mView.interfaceManager.showMessage(message)
    }
}