package com.asprojecteam.cs_cartadministrator.Controllers

import com.asprojecteam.cs_cartadministrator.Classes.Currency
import com.asprojecteam.cs_cartadministrator.Classes.Order
import com.asprojecteam.cs_cartadministrator.Classes.OrderStatus
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentDashboard
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentOrders
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.DataLoader
import com.miguelcatalan.materialsearchview.MaterialSearchView

/**
 * Controller class of the FragmentOrder
 *
 * @param mView
 */
class OrdersController (val mView: FragmentOrders)
{
    /**
     * Primary currency
     */
    lateinit var currency: Currency

    /**
     * List of the loaded order statuses
     */
    private var orderStatusesList = mutableMapOf<String, OrderStatus>()

    /**
     * API loader
     */
    private var dataLoader: DataLoader = DataLoader(mView.requireContext())

    /**
     * Page of the loaded orders
     */
    private var ordersPage = 1

    /**
     * Load orders from the API
     *
     * @param page Number of the loaded page
     */
    fun loadOrders(page: Int = 1)
    {
        ordersPage = page

        val itemsPerPage = 20

        val params = LoadParameters(page, itemsPerPage)

        dataLoader.loadOrderStatuses(::onLoadOrderStatuses, ::onFailLoadOrders, params)
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    fun onBackPressed(): Boolean
    {
        if (mView.interfaceManager.onBackPressed()) {
            return true
        }

        return false
    }

    fun onOrdersSearchQueryTextListener(): MaterialSearchView.OnQueryTextListener
    {
        return object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean { //Do some magic
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean { //Do some magic
                mView.searchText = newText!!

                val orderId = newText.toIntOrNull()

                if (orderId != null) {
                    mView.interfaceManager.showLoader()

                    dataLoader.loadOrderById(
                        ::onLoadSearchOrders,
                        mView.interfaceManager::showNoDataError,
                        orderId
                    )
                } else {
                    mView.mPresenter.showDefaultOrderList()
                }
                return false
            }
        }
    }

    fun onOrdersSearchViewListener(): MaterialSearchView.SearchViewListener
    {
        return object: MaterialSearchView.SearchViewListener {
            override fun onSearchViewClosed() {
                mView.mPresenter.showDefaultOrderList()
            }

            override fun onSearchViewShown() {

            }
        }
    }

    /**
     * Actions on successful loaded orders
     *
     * @param ordersList List of loaded orders
     * @param params     Params to load
     */
    private fun onLoadOrders(ordersList: MutableList<Order>, params: LoadParameters): Unit
    {
        if (ordersList.count() == 0) {
            mView.interfaceManager.showNoDataError()
        } else {
            mView.mPresenter.showOrders(ordersList, params, orderStatusesList)
        }
    }

    /**
     * Actions on successful load searched orders
     *
     * @param order Loaded order
     */
    private fun onLoadSearchOrders(order: Order?): Unit
    {
        when {
            order == null -> {
                mView.interfaceManager.showNoDataError()
            }
            mView.searchText == order.id.toString() -> {
                mView.mPresenter.showSearchOrder(order, orderStatusesList)
            }
            else -> {
                mView.mPresenter.showDefaultOrderList()
            }
        }
    }

    /**
     * Actions on successful load order statuses
     *
     * @param orderStatusList List of order statuses
     * @param loadParams      Params to load
     */
    private fun onLoadOrderStatuses(orderStatusList: MutableList<OrderStatus>, loadParams: LoadParameters): Unit
    {
        orderStatusList.forEach{
            orderStatusesList[it.status] = it
        }
        dataLoader.loadCurrency(::onLoadCurrency, ::onFailLoadOrders)
    }

    /**
     * Actions on successful load primary currency
     *
     * @param primaryCurrency Currency which was loaded
     */
    private fun onLoadCurrency(primaryCurrency: Currency)
    {
        currency = primaryCurrency
        val params = LoadParameters(ordersPage, 20)
        dataLoader.loadOrders(::onLoadOrders, params)
    }

    /**
     * Actions after orders list data was failed to load
     */
    private fun onFailLoadOrders()
    {
        mView.interfaceManager.hideLoader()
        mView.interfaceManager.showNoDataError()
    }
}