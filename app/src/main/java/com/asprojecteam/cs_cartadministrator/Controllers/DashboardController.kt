package com.asprojecteam.cs_cartadministrator.Controllers

import android.view.View
import com.asprojecteam.cs_cartadministrator.Classes.*
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentDashboard
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.DataLoader
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.OrderStatuses
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.UserTypes

/**
 * Controller class of the FragmentLogin
 *
 * @param mView
 */
class DashboardController(
    val mView: FragmentDashboard
) {
    /**
     * API loader
     */
    private var dataLoader: DataLoader = DataLoader(mView.requireContext())

    /**
     * Loads dashboard stats
     */
    fun loadStatistic()
    {
        var params = LoadParameters(1, 1)
        dataLoader.loadProducts(::onLoadProductsStats, ::onFailLoadProductsStats, params)
        dataLoader.loadCategories(::onLoadCategoriesStats, fun() {}, params)
        dataLoader.loadOrders(::onLoadOrdersStats, params)

        params.status = OrderStatuses.STATUS_OPEN
        dataLoader.loadOrders(::onLoadOpenOrdersStats, params)
        params.status = null

        params.userType = UserTypes.CUSTOMER
        dataLoader.loadUsers(
            ::onLoadUsers,
            ::onFailLoadUsersStats,
            params,
            false
        )

        params = LoadParameters(1, 1)
        params.amountTo = 0
        dataLoader.loadProducts(::onLoadOutOfStockProductStats, ::onFailLoadOutOfStockProductStats, params)
    }

    /**
     * Callback function on orders stats was loaded
     *
     * @param ordersList List of the orders
     * @param params     Params of the loaded orders
     */
    private fun onLoadOrdersStats(ordersList: MutableList<Order>, params: LoadParameters): Unit
    {
        mView.ordersNumberText.text = params.totalItems.toString()
        mView.ordersNumberText.visibility = View.VISIBLE
        mView.ordersNumberLoader.visibility = View.GONE
    }

    /**
     * Callback function on open orders stats was loaded
     *
     * @param ordersList List of the open orders
     * @param params     Params of the loaded orders
     */
    private fun onLoadOpenOrdersStats(ordersList: MutableList<Order>, params: LoadParameters)
    {
        mView.openOrdersNumberText.text = params.totalItems.toString()
        mView.openOrdersNumberText.visibility = View.VISIBLE
        mView.openOrdersNumberLoader.visibility = View.GONE
    }

    /**
     * Callback function on products stats was loaded
     *
     * @param productList List of the products
     * @param params      Params of the loaded products
     * @param isSearch    Flag if it is Search load
     */
    private fun onLoadProductsStats(productList: MutableList<Product>, params: LoadParameters, isSearch: Boolean)
    {
        mView.productsNumberText.text = params.totalItems.toString()
        mView.productsNumberText.visibility = View.VISIBLE
        mView.productsNumberLoader.visibility = View.GONE
    }

    /**
     * Callback function on products load was failed
     */
    private fun onFailLoadProductsStats()
    {
        mView.productsNumberText.text = "NaN"
        mView.productsNumberText.visibility = View.VISIBLE
        mView.productsNumberLoader.visibility = View.GONE
    }

    /**
     * Callback function on out of stock products stats was loaded
     *
     * @param productList List of the out of stock products
     * @param params      Params of the loaded products
     * @param isSearch    Flag if it is Search load
     */
    private fun onLoadOutOfStockProductStats(productList: MutableList<Product>, params: LoadParameters, isSearch: Boolean)
    {
        mView.outOfStockProductsNumberText.text = params.totalItems.toString()
        mView.outOfStockProductsNumberText.visibility = View.VISIBLE
        mView.outOfStockProductsNumberLoader.visibility = View.GONE
    }

    /**
     * Callback function on out of stock products load was failed
     */
    private fun onFailLoadOutOfStockProductStats()
    {
        mView.outOfStockProductsNumberText.text = "NaN"
        mView.outOfStockProductsNumberText.visibility = View.VISIBLE
        mView.outOfStockProductsNumberLoader.visibility = View.GONE
    }

    /**
     * Callback function on users stats was loaded
     *
     * @param userList List of the users
     * @param params   Params of the loaded products
     * @param isSearch Flag if it is Search load
     */
    private fun onLoadUsers(userList: MutableList<User>, params: LoadParameters, isSearch: Boolean)
    {
        mView.customersNumberText.text = params.totalItems.toString()
        mView.customersNumberText.visibility = View.VISIBLE
        mView.customersNumberLoader.visibility = View.GONE
    }

    /**
     * Callback function on users load was failed
     */
    private fun onFailLoadUsersStats()
    {
        mView.customersNumberText.text = "NaN"
        mView.customersNumberText.visibility = View.VISIBLE
        mView.customersNumberLoader.visibility = View.GONE
    }

    /**
     * Callback function on categories stats was loaded
     *
     * @param categoryList List of the categories
     * @param params       Params of the loaded categories
     */
    private fun onLoadCategoriesStats(categoryList: MutableList<Category>, params: LoadParameters, isSearch: Boolean)
    {
        mView.categoriesNumberText.text = params.totalItems.toString()
        mView.categoriesNumberText.visibility = View.VISIBLE
        mView.categoriesNumberLoader.visibility = View.GONE
    }
}