package com.asprojecteam.cs_cartadministrator.Controllers

import android.content.ContentValues.TAG
import android.util.Log
import com.asprojecteam.cs_cartadministrator.Classes.Login
import com.asprojecteam.cs_cartadministrator.Classes.Setting
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentLogin
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentQrcode
import com.asprojecteam.cs_cartadministrator.Presenters.LoginPresenter
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.Common
import com.asprojecteam.cs_cartadministrator.Tygh.Data.Saver
import com.asprojecteam.cs_cartadministrator.Tygh.DataLoader
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.MultiVendorSettings
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.ParamIds
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Login.Results
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Saver.Types
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.StoreEditions


/**
 * Controller class of the FragmentLogin
 *
 * @param mView
 */
class LoginController (val mView: FragmentLogin)
{
    /**
     * API loader
     */
    private var dataLoader: DataLoader = DataLoader(mView.requireContext())

    /**
     * Send a request to login
     */
    fun login()
    {
        mView.mPresenter.showProgressbar()

        val loginData = Login(
            mView.loginLogin.text.toString(),
            mView.loginPassword.text.toString(),
            mView.loginWebUrl.text.toString()
        )

        if (
            mView.loginLogin.text.isEmpty()
            || mView.loginPassword.text.isEmpty()
            || mView.loginWebUrl.text.isEmpty()
        ) {
            mView.mPresenter.hideProgressbar()
            mView.mPresenter.showText("One of the fields is empty.") //TODO: Add language variable
            return
        }

        dataLoader.login(
            mView.mController::onSuccessLogin,
            mView.mController::onFailedLogin,
            loginData,
            true
        )
    }

    /**
     * Action on QR scanner button was clicked
     */
    fun onButtonQrClick() {
        openQrScanPage()
    }

    /**
     * Action on Login button was clicked
     */
    fun onButtonLoginClick() {
        login()
    }

    /**
     * Action on Login to demo store button was clicked
     */
    fun onButtonLoginDemoStoreClick() {
       loginToDemoStore()
    }

    /**
     * Parse string from the QR scan result
     *
     * @param qrString Scanned QR string
     *
     * @return List of strings
     */
    fun parseQrCodeResult(qrString: String): List<String> {
        val qrArray = qrString.split('|')

        val result: MutableList<String> = mutableListOf()

        if (qrArray.count() == 4) {
            val apiKeyArray = qrArray[0].split('=')
            val emailArray = qrArray[1].split('=')
            val urlArray = qrArray[2].split('=')

            if (
                apiKeyArray.count() == 2
                && apiKeyArray[0] == Results.API_KEY
                && emailArray.count() == 2
                && emailArray[0] == Results.EMAIL
                && urlArray.count() == 2
                && urlArray[0] == Results.URL
            ) {
                result.add(apiKeyArray[1])
                result.add(emailArray[1])
                result.add(urlArray[1])
            }
        }

        return result
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    fun onBackPressed(): Boolean
    {
        return true
    }

    /**
     * Loads store edition
     */
    private fun loadStoreEdition()
    {
        val onFailLoad = fun () {
            mView.mainActivity.mController.changeAddonStatus(false)
            loadSettings()
        }

        val onSuccessLoad = fun (edition: String) {
            if (edition == StoreEditions.MULTIVENDOR_STR) {
                mView.mainActivity.mController.changeStoreEdition(StoreEditions.MULTIVENDOR)
            }
            mView.mainActivity.mController.changeAddonStatus(true)
        }

        dataLoader.getStoreEdition(onSuccessLoad, onFailLoad)
    }

    /**
     * Loads settings
     */
    private fun loadSettings()
    {
        val params = LoadParameters(1, 100)

        dataLoader.loadSettings(::onLoadSettings, ::onFailLoadSettings, params)
    }

    /**
     * Callback function on settings were loaded
     *
     * @param settingsList List of settings
     * @param params       Params of the loaded settings
     */
    private fun onLoadSettings(settingsList: MutableMap<String, Setting>, params: LoadParameters): Unit
    {
        var needLoadMoreSettings = true
        settingsList.forEach{
            val settingName = it.key
            if (MultiVendorSettings.settings.contains(settingName)) {
                mView.mainActivity.mController.changeStoreEdition(StoreEditions.MULTIVENDOR)
                needLoadMoreSettings = false
                return@forEach
            }
        }

        if (needLoadMoreSettings) {
            if (params.page * params.itemsPerPage >= params.totalItems!!) {
                needLoadMoreSettings = false
            } else {
                params.page = params.page + 1
                dataLoader.loadSettings(::onLoadSettings, ::onFailLoadSettings, params)
            }
        }

        if (!needLoadMoreSettings) {
            mView.mPresenter.showBottomMenu()
            mView.mPresenter.unlockSideBarMenu()
            mView.mPresenter.openHomePage()
        }
    }

    /**
     * Callback function on settings load was failed
     */
    private fun onFailLoadSettings()
    {
        mView.mPresenter.hideProgressbar()
        mView.mPresenter.showText("Failed to login. Internet connection error.")
    }

    /**
     * Connect to the demo  store
     */
    private fun loginToDemoStore() {
        mView.mPresenter.showProgressbar()

        val loginData = Login(
            "admin@example.com",
            "43O3Gq3E0X37tCQPY4S7y1R38ur1b2U1",
            "http://62.109.25.6/mve/"
        )

        dataLoader.login(
            mView.mController::onSuccessLogin,
            mView.mController::onFailedLogin,
            loginData,
            false
        )
    }

    /**
     * Opens QR code scanner
     */
    private fun openQrScanPage()
    {
        Common.hideKeyboard(mView.requireContext(), mView.requireView())

        val fragmentQrcode = FragmentQrcode()
        val fragmentTrans = mView.requireActivity().supportFragmentManager.beginTransaction()
        fragmentTrans.addToBackStack(null)
        fragmentTrans.replace(R.id.main_container, fragmentQrcode)
        fragmentTrans.commit()
    }

    /**
     * Actions after success login
     *
     * @param login    Login email
     * @param password Password
     * @param url      Store url
     */
    private fun onSuccessLogin(loginData: Login, isSaveLogin: Boolean)
    {
        if (isSaveLogin) {
            val saver = Saver(Types.SETTINGS_LOGIN, mView.requireContext())

            saver.saveString(Types.LOGIN_KEY_EMAIL, loginData.login)
            saver.saveString(Types.LOGIN_KEY_API, loginData.password)
            saver.saveString(Types.LOGIN_KEY_URL, loginData.url)
            saver.saveBoolean(Types.LOGIN_SUCCESSFUL, true)
        }

        loadStoreEdition()
    }

    /**
     * Actions after failed login
     *
     * @param responseCode Code of the error
     */
    private fun onFailedLogin(responseCode: Int)
    {
        val saver = Saver(Types.SETTINGS_LOGIN, mView.requireContext())
        saver.saveBoolean(Types.LOGIN_SUCCESSFUL, false)

        mView.mPresenter.hideProgressbar()

        when (responseCode) {
            ParamIds.ERROR_FAILED_TO_LOAD -> {
                mView.mPresenter.showText("Failed to login. Internet connection error.")
            }
            404 -> {
                mView.mPresenter.showText("404 error")
            }
            else -> {
                mView.mPresenter.showText("Error $responseCode")
            }
        }
    }
}