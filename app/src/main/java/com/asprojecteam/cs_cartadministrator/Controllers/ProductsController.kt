package com.asprojecteam.cs_cartadministrator.Controllers

import android.os.Bundle
import android.view.MenuItem
import com.asprojecteam.cs_cartadministrator.Classes.Currency
import com.asprojecteam.cs_cartadministrator.Classes.Product
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentProductDetailed
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentProducts
import com.asprojecteam.cs_cartadministrator.Presenters.ProductsPresenter
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.DataLoader
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles.ProductBundle
import com.asprojecteam.cs_cartadministrator.Tygh.ITEMS_PER_PAGE
import com.miguelcatalan.materialsearchview.MaterialSearchView

/**
 * Controller class of the FragmentProducts
 *
 * @param mView FragmentProducts
 */
class ProductsController (
    val mView: FragmentProducts
) {
    /**
     * Primary currency
     */
    lateinit var currency: Currency

    /**
     * API loader
     */
    private var dataLoader: DataLoader = DataLoader(mView.requireContext())

    /**
     * Load products
     *
     * @param page       Page of the products
     * @param loadParams Params to load products
     * @param isSearch   Flag if it serach load
     */
    fun loadProducts(page: Int = 1, loadParams: LoadParameters? = null, isSearch: Boolean = false)
    {
        val params: LoadParameters?
        params = loadParams ?: LoadParameters(page, ITEMS_PER_PAGE)
        params.page = page
        dataLoader.loadProducts(::onLoadProducts, ::onFailLoadProducts, params, isSearch)
    }

    /**
     * Loads necessary data for this page
     */
    fun loadData()
    {
        mView.interfaceManager.showLoader()

        dataLoader.loadCurrency(::onLoadCurrency, ::onFailLoadProducts)
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    fun onBackPressed(): Boolean
    {
        if (mView.interfaceManager.onBackPressed()) {
            return true
        }

        return false
    }

    /**
     * Try to search product if search query text changed
     */
    fun onSearchQueryTextListener(): MaterialSearchView.OnQueryTextListener
    {
        return object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean { //Do some magic
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean { //Do some magic
                mView.searchText = newText!!

                if (newText != "") {
                    mView.interfaceManager.showLoader()

                    val params = LoadParameters(1, ITEMS_PER_PAGE)
                    params.query = newText
                    dataLoader.loadProducts(
                        ::onLoadProducts,
                        mView.interfaceManager::showNoDataError,
                        params,
                        true
                    )
                } else {
                    mView.mPresenter.showDefaultList()
                }
                return false
            }
        }
    }

    /**
     * Shows default product list if the search view was closed
     */
    fun onSearchViewListener(): MaterialSearchView.SearchViewListener
    {
        return object: MaterialSearchView.SearchViewListener {
            override fun onSearchViewClosed() {
                mView.mPresenter.showDefaultList()
            }

            override fun onSearchViewShown() {

            }
        }
    }

    /**
     * Open create product page
     */
    fun onCreateMenuItemClicked(): MenuItem.OnMenuItemClickListener
    {
        return MenuItem.OnMenuItemClickListener {
            val bundle = Bundle()
            bundle.putInt(ProductBundle.PRODUCT_ID, 0)

            openProductDetailedFragment(bundle)

            true
        }
    }

    /**
     * Set product id and open product detailed page
     *
     * @param item Product data
     */
    fun onProductClicked(item: Product, position: Int){
        mView.openedProductPosition = position

        val bundle = Bundle()
        bundle.putInt(ProductBundle.PRODUCT_ID, item.id)
        bundle.putString(ProductBundle.PRODUCT_NAME, item.product)

        openProductDetailedFragment(bundle)
    }

    /**
     * Opens product detailed page
     *
     * @param bundle Bundle with product id and product name
     */
    private fun openProductDetailedFragment(bundle: Bundle)
    {
        val fragmentProductDetailed = FragmentProductDetailed()
        val fragmentTrans = mView.requireActivity().supportFragmentManager

        fragmentProductDetailed.arguments = bundle

        fragmentTrans.beginTransaction()
            .replace(R.id.main_container, fragmentProductDetailed)
            .addToBackStack(null)
            .commit()
    }

    /**
     * Actions after primary currency was loaded
     *
     * @param primaryCurrency
     */
    private fun onLoadCurrency(primaryCurrency: Currency)
    {
        currency = primaryCurrency
        loadProducts()
    }

    /**
     * Actions after products was loaded
     *
     * @param productList Product list
     * @param params      Params of the loaded products
     * @param isSearch    Flag if it search load
     */
    private fun onLoadProducts(productList: MutableList<Product>, params: LoadParameters, isSearch: Boolean)
    {
        if (productList.count() == 0) {
            mView.interfaceManager.showNoDataError()
        } else {
            mView.mPresenter.showProducts(productList, params, isSearch, currency)
        }
    }

    /**
     * Actions after orders list data was failed to load
     */
    private fun onFailLoadProducts()
    {
        mView.interfaceManager.hideLoader()
        mView.interfaceManager.showNoDataError()
    }
}