package com.asprojecteam.cs_cartadministrator.Controllers

import com.asprojecteam.cs_cartadministrator.Classes.User
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentUsers
import com.asprojecteam.cs_cartadministrator.Presenters.UsersPresenter
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.DataLoader
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.UserTypes
import com.asprojecteam.cs_cartadministrator.Tygh.ITEMS_PER_PAGE
import com.google.android.material.tabs.TabLayout
import com.miguelcatalan.materialsearchview.MaterialSearchView

/**
 * Controller class of the FragmentUsers
 *
 * @param mView FragmentUsers
 */
class UsersController (
    val mView: FragmentUsers
) {
    /** API data loader */
    private var dataLoader: DataLoader = DataLoader(mView.requireContext())

    /**
     * Load users from the API
     *
     * @param page     Page of the search params
     * @param userType User typ to load Admin|Customer|Vendor
     * @param isSearch True if it is search load, else false
     */
    fun loadUsers(page: Int = 1, userType: String = UserTypes.CUSTOMER, isSearch: Boolean = false)
    {
        val itemsPerPage = 20

        val params = LoadParameters(page, itemsPerPage)
        params.userType = userType
        dataLoader.loadUsers(
            ::onLoadUsers,
            mView.interfaceManager::showNoDataError,
            params,
            isSearch
        )
    }

    /**
     * Change the user list if while selected tab layout
     */
    fun onTabLayoutSelected(): TabLayout.OnTabSelectedListener {
        return object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.text == mView.getString(R.string.administrators)) {
                    mView.currentList = UserTypes.ADMIN
                    mView.mPresenter.showAdminList()
                } else {
                    mView.currentList = UserTypes.CUSTOMER
                    mView.mPresenter.showCustomerList()
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        }
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    fun onBackPressed(): Boolean
    {
        if (mView.interfaceManager.onBackPressed()) {
            return true
        }

        return false
    }

    /**
     * Callback function if the users were loaded with success
     *
     * @param usersList List of the loaded users
     * @param params    Params of the loaded data
     * @param isSearch  True if it is search load, else false
     */
    private fun onLoadUsers(usersList: MutableList<User>, params: LoadParameters, isSearch: Boolean)
    {
        if (usersList.count() == 0) {
            mView.interfaceManager.showNoDataError()
        } else {
            if (params.userType == UserTypes.CUSTOMER) {
                mView.mPresenter.showUsers(usersList, params, isSearch)
            } else if (params.userType == UserTypes.ADMIN) {
                mView.mPresenter.showUsers(usersList, params, isSearch)
            }
        }
    }

    /**
     * Try to find users while the query text is typed
     */
    fun onSearchQueryTextListener(): MaterialSearchView.OnQueryTextListener
    {
        return object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean { //Do some magic
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean { //Do some magic
                mView.searchText = newText!!

                if (newText != "") {
                    mView.interfaceManager.showLoader()

                    val params = LoadParameters(1, ITEMS_PER_PAGE)

                    params.name = newText
                    params.userType = mView.currentList

                    dataLoader.loadUsers(
                        ::onLoadUsers,
                        mView.interfaceManager::showNoDataError,
                        params,
                        true
                    )
                } else {
                    when(mView.currentList) {
                        UserTypes.CUSTOMER -> mView.mPresenter.showCustomerList()
                        UserTypes.ADMIN    -> mView.mPresenter.showAdminList()
                    }
                }
                return false
            }
        }
    }

    /**
     * Open default user list after search view was closed
     */
    fun onSearchViewListener(): MaterialSearchView.SearchViewListener
    {
        return object: MaterialSearchView.SearchViewListener {
            override fun onSearchViewClosed() {
                when(mView.currentList) {
                    UserTypes.CUSTOMER -> mView.mPresenter.showCustomerList()
                    UserTypes.ADMIN    -> mView.mPresenter.showAdminList()
                }
            }

            override fun onSearchViewShown() {

            }
        }
    }
}