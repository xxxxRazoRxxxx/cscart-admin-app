package com.asprojecteam.cs_cartadministrator.Controllers

import android.view.View
import com.asprojecteam.cs_cartadministrator.Classes.Order
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentOrderDetailed
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.DataLoader
import com.google.android.material.tabs.TabLayout

/**
 * Controller of the FragmentOrderDetailed
 *
 * @param mView
 */
class OrderDetailedController (
    val mView: FragmentOrderDetailed
) {
    /**
     * API loader
     */
    private var dataLoader: DataLoader = DataLoader(mView.requireContext())

    /**
     * Tab layout switcher
     */
    fun onTabLayoutSelected(): TabLayout.OnTabSelectedListener {
        return object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> {
                        mView.mPresenter.showOrderInformationBlock()
                    }
                    1 -> {
                        mView.mPresenter.showPaymentInformationBlock()
                    }
                    2 -> {
                        mView.mPresenter.showCustomerInformationBlock()
                    }
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        }
    }

    /**
     * Actions after UpdateStatus button was clicked
     *
     * @param order Order to update
     */
    fun onUpdateStatusButtonClicked(order: Order)
    {
        val status = mView.statusSelect.selectedItem.toString()

        mView.orderStatusesList.forEach {(_, orderStatus) ->
            if (orderStatus.description == status) {
                order.status = orderStatus.status
                return@forEach
            }
        }

        dataLoader.updateOrder(::onOrderUpdatedSuccess, ::onOrderUpdatedFail, order)
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    fun onBackPressed(): Boolean
    {
        mView.requireActivity().supportFragmentManager.popBackStack()

        return true
    }

    /**
     * Actions after order was successfully updated
     *
     * @param order Order which was updated
     */
    private fun onOrderUpdatedSuccess(order: Order)
    {
        val message = mView.resources.getString(R.string.order_was_updated)
        mView.interfaceManager.showMessage(message)
    }

    /**
     * Actions after order update was failed
     */
    private fun onOrderUpdatedFail()
    {
        mView.interfaceManager.showMessage("Order wasn't updated")

        mView.mPresenter.setStatusToOrderSelect(
            mView.mPresenter.order.statusParams.description
        )
    }
}