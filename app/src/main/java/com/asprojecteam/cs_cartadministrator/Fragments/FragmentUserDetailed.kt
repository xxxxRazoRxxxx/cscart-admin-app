package com.asprojecteam.cs_cartadministrator.Fragments

import android.os.Bundle
import android.view.*
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RadioButton
import android.widget.RelativeLayout
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.asprojecteam.cs_cartadministrator.Classes.User
import com.asprojecteam.cs_cartadministrator.Controllers.ProductDetailedController
import com.asprojecteam.cs_cartadministrator.Controllers.UserDetailedController
import com.asprojecteam.cs_cartadministrator.Presenters.ProductDetailedPresenter
import com.asprojecteam.cs_cartadministrator.Presenters.UserDetailedPresenter

import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles.ProductBundle
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles.UserBundle
import com.asprojecteam.cs_cartadministrator.Tygh.Inrefaces.OnBackPressedListener
import com.asprojecteam.cs_cartadministrator.Tygh.InterfaceManager.DetailedEntityInterfaceManager

/**
 * FragmentUserDetailed class
 */
class FragmentUserDetailed : Fragment(), OnBackPressedListener {
    /** Interface manager */
    var interfaceManager = DetailedEntityInterfaceManager()

    lateinit var mPresenter: UserDetailedPresenter
    lateinit var mController: UserDetailedController

    lateinit var toolbar: Toolbar
    lateinit var loader: ProgressBar
    lateinit var viewer: RelativeLayout
    lateinit var viewerNoData: RelativeLayout

    lateinit var userFirstNameEditText: EditText
    lateinit var userLastNameEditText: EditText
    lateinit var userEmailEditText: EditText
    lateinit var userPhoneEditText: EditText

    lateinit var userStatusRadioButtonActive: RadioButton
    lateinit var userStatusRadioButtonDisabled: RadioButton

    lateinit var user: User

    var userId: Int? = null
    var userName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireArguments().let {
            userId = it.getInt(UserBundle.USER_ID)
            userName = it.getString(UserBundle.USER_NAME)!!
        }
    }

    /**
     * Actions on create View
     *      - creates controller
     *      - creates presenter
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_user_detailed, container, false)

        mPresenter = UserDetailedPresenter(this)
        mController = UserDetailedController(this)

        mPresenter.initializeComponents(view)
        mPresenter.initializeController()

        setHasOptionsMenu(true)

        return view
    }

    /**
     * Actions on create options menu
     *      - hides search button
     *      - adds save button
     *
     * @param menu
     * @param menuInflater
     *
     * @return Boolean
     */
    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        val searchButton = menu.findItem(R.id.action_search)
        val saveButton = menu.findItem(R.id.action_save)
        searchButton.isVisible = false
        saveButton.isVisible = true
        saveButton.setOnMenuItemClickListener(mController.onSaveMenuItemClicked())
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    override fun onBackPressed(): Boolean {
        return mController.onBackPressed()
    }
}
