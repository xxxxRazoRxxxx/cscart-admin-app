package com.asprojecteam.cs_cartadministrator.Fragments


import android.os.Bundle
import android.view.*
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import android.widget.RelativeLayout
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters.ProductsAdapter
import com.asprojecteam.cs_cartadministrator.Controllers.ProductsController
import com.asprojecteam.cs_cartadministrator.MainActivity
import com.asprojecteam.cs_cartadministrator.Presenters.ProductsPresenter

import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Inrefaces.OnBackPressedListener
import com.asprojecteam.cs_cartadministrator.Tygh.InterfaceManager.ListPageInterfaceManager
import com.miguelcatalan.materialsearchview.MaterialSearchView
import kotlinx.android.synthetic.main.fragment_products.view.*

/**
 * FragmentProductDetailed class
 */
class FragmentProducts : Fragment(), OnBackPressedListener {
    /** Interface manager */
    var interfaceManager = ListPageInterfaceManager()

    lateinit var viewer: RelativeLayout
    lateinit var viewerNoData: RelativeLayout
    lateinit var loader: ProgressBar

    lateinit var productsList: RecyclerView
    lateinit var adapter: ProductsAdapter
    lateinit var searchAdapter: ProductsAdapter

    lateinit var mPresenter: ProductsPresenter
    lateinit var mController: ProductsController

    lateinit var searchView: MaterialSearchView
    lateinit var searchText: String

    lateinit var toolbar: Toolbar

    var openedProductPosition: Int = 0

    /**
     * Actions on create View
     *  - creates controller
     *  - creates presenter
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_products, container, false)

        mPresenter = ProductsPresenter(this)
        mController = ProductsController(this)

        mPresenter.initializeComponents(view)

        mPresenter.initializeRepository()

        setHasOptionsMenu(true)

        return view
    }

    /**
     * Checks if adapter was initialized
     *
     * @return Boolean
     */
    fun isAdapterInitialized(): Boolean
    {
        return this::adapter.isInitialized;
    }

    /**
     * Actions on create options menu
     *      - adds search button
     *
     * @param menu
     * @param menuInflater
     *
     * @return Boolean
     */
    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        val searchButton = menu.findItem(R.id.action_search)
        val createButton = menu.findItem(R.id.action_create)

        createButton.isVisible = true

        createButton.setOnMenuItemClickListener(mController.onCreateMenuItemClicked())

        this.searchView.setMenuItem(searchButton)
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    override fun onBackPressed(): Boolean {
        return mController.onBackPressed()
    }

    /**
     * Actions on Resume:
     *  - Changes changed product
     */
    override fun onResume() {
        val activity = requireActivity() as MainActivity
        if (activity.changedProduct != null) {
            activity.changedProduct?.let { (productsList.adapter as ProductsAdapter).changeProduct(it, openedProductPosition) }
            activity.changedProduct = null
        }

        super.onResume()
    }
}
