package com.asprojecteam.cs_cartadministrator.Fragments

import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import com.asprojecteam.cs_cartadministrator.Classes.Category
import com.asprojecteam.cs_cartadministrator.Classes.Currency
import com.asprojecteam.cs_cartadministrator.Classes.Product
import com.asprojecteam.cs_cartadministrator.Controllers.OrderDetailedController
import com.asprojecteam.cs_cartadministrator.Controllers.ProductDetailedController
import com.asprojecteam.cs_cartadministrator.MainActivity
import com.asprojecteam.cs_cartadministrator.Presenters.OrderDetailedPresenter
import com.asprojecteam.cs_cartadministrator.Presenters.ProductDetailedPresenter

import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles.ProductBundle
import com.asprojecteam.cs_cartadministrator.Tygh.Inrefaces.OnBackPressedListener
import com.asprojecteam.cs_cartadministrator.Tygh.InterfaceManager.DetailedEntityInterfaceManager
import com.google.android.material.textfield.TextInputLayout

/**
 * FragmentProductDetailed class
 */
class FragmentProductDetailed : Fragment(), OnBackPressedListener {
    /** Interface manager */
    var interfaceManager = DetailedEntityInterfaceManager()

    lateinit var mPresenter: ProductDetailedPresenter
    lateinit var mController: ProductDetailedController

    lateinit var toolbar: Toolbar
    lateinit var loader: ProgressBar
    lateinit var viewer: RelativeLayout
    lateinit var viewerNoData: RelativeLayout

    lateinit var productCodeEditText: EditText
    lateinit var productNameEditText: EditText
    lateinit var productPriceEditText: EditText
    lateinit var productPriceLayout: TextInputLayout
    lateinit var productAmountEditText: EditText

    lateinit var productStatusRadioButtonActive: RadioButton
    lateinit var productStatusRadioButtonHidden: RadioButton
    lateinit var productStatusRadioButtonDisabled: RadioButton

    lateinit var addCategoryButton: Button

    lateinit var productImageList: RecyclerView
    lateinit var productCategoryList: LinearLayout

    lateinit var product: Product
    lateinit var currency: Currency

    var productId: Int? = null
    var productName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireArguments().let {
            productId = it.getInt(ProductBundle.PRODUCT_ID)

            if (it.getString(ProductBundle.PRODUCT_NAME) != null) {
                productName = it.getString(ProductBundle.PRODUCT_NAME)!!
            }
        }
    }

    /**
     * Actions on create View
     *  - creates controller
     *  - creates presenter
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_product_detailed, container, false)

        mPresenter = ProductDetailedPresenter(this)
        mController = ProductDetailedController(this)

        mPresenter.initializeComponents(view)

        if (!this::product.isInitialized) {
            mController.loadProduct()
        }

        setHasOptionsMenu(true)

        return view
    }

    /**
     * Actions on create options menu
     *      - hides search button
     *      - adds save button
     *
     * @param menu
     * @param menuInflater
     *
     * @return Boolean
     */
    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.toolbar_menu, menu)

        val searchButton = menu.findItem(R.id.action_search)
        val saveButton = menu.findItem(R.id.action_save)

        searchButton.isVisible = false
        saveButton.isVisible = true

        saveButton.setOnMenuItemClickListener(mController.onSaveMenuItemClicked())
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    override fun onBackPressed(): Boolean {
        return mController.onBackPressed()
    }

    /**
     * Actions on Resume:
     *  - Show product data
     *  - Add selected categories to product
     */
    override fun onResume() {
        val activity = requireActivity() as MainActivity
        if (activity.selectedCategories.count() > 0) {
            activity.selectedCategories.forEach {
                val category = Category()
                category.id = it.key
                category.category = it.value

                product.categoryList.add(category)
                product.categoryIds.add(it.key)
            }
        }

        if (this::product.isInitialized) {
            mPresenter.showProductInfo(product, false)
        }

        super.onResume()
    }
}
