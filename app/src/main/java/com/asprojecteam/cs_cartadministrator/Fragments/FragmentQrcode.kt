package com.asprojecteam.cs_cartadministrator.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.asprojecteam.cs_cartadministrator.Controllers.QrcodeController
import com.asprojecteam.cs_cartadministrator.Presenters.QrcodePresenter
import com.asprojecteam.cs_cartadministrator.Tygh.Inrefaces.OnBackPressedListener
import me.dm7.barcodescanner.zxing.ZXingScannerView
import com.google.zxing.Result

/**
 * FragmentQrcode class
 */
class FragmentQrcode : Fragment(), ZXingScannerView.ResultHandler, OnBackPressedListener {

    lateinit var mPresenter: QrcodePresenter
    lateinit var mController: QrcodeController
    lateinit var mScannerView: ZXingScannerView

    /**
     * Actions on create View
     *      - creates controller
     *      - creates presenter
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     *
     * @return View?
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mPresenter = QrcodePresenter(this)
        mController = QrcodeController(this)

        mScannerView = mPresenter.getScannerView()!!

        return mScannerView
    }

    /**
     * On resume fragment actions
     */
    override fun onResume() {
        super.onResume()
        mScannerView.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView.startCamera()          // Start camera on resume
    }

    /**
     * On pause fragment actions
     */
    override fun onPause() {
        super.onPause()
        mScannerView.stopCamera()           // Stop camera on pause
    }

    /**
     * Handles result of the QR scan
     */
    override fun handleResult(rawResult: Result?) {
        mController.handleQrScanResult(rawResult)
    }

    /**
     * Actions on request permissions result
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    override fun onRequestPermissionsResult(
        requestCode: Int ,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        mController.onRequestPermissionsResult (
            requestCode,
            grantResults
        )
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    override fun onBackPressed(): Boolean {
        return mController.onBackPressed()
    }
}
