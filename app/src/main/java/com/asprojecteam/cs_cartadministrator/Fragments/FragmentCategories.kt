package com.asprojecteam.cs_cartadministrator.Fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters.CategoriesAdapter
import com.asprojecteam.cs_cartadministrator.Controllers.CategoriesController
import com.asprojecteam.cs_cartadministrator.Presenters.CategoriesPresenter
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles.GeneralBundle
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles.ProductBundle
import com.asprojecteam.cs_cartadministrator.Tygh.Inrefaces.OnBackPressedListener
import com.asprojecteam.cs_cartadministrator.Tygh.InterfaceManager.ListPageInterfaceManager
import com.miguelcatalan.materialsearchview.MaterialSearchView

/**
 * FragmentCategories class
 */
class FragmentCategories : Fragment(), OnBackPressedListener {

    /** Interface manager */
    var interfaceManager = ListPageInterfaceManager()

    lateinit var viewer: RelativeLayout
    lateinit var viewerNoData: RelativeLayout
    lateinit var loader: ProgressBar

    lateinit var categoriesList: RecyclerView
    lateinit var adapter: CategoriesAdapter
    lateinit var searchAdapter: CategoriesAdapter

    lateinit var mPresenter: CategoriesPresenter
    lateinit var mController: CategoriesController

    lateinit var searchView: MaterialSearchView
    lateinit var searchText: String

    lateinit var checkButton: MenuItem

    lateinit var toolbar: Toolbar

    var selectableCategories = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            requireArguments().let {
                selectableCategories = it.getBoolean(GeneralBundle.ENABLE_SELECT)
            }
        }
    }

    /**
     * Actions on create View
     *  - creates controller
     *  - creates presenter
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_categories, container, false)

        mPresenter = CategoriesPresenter(this)
        mController = CategoriesController(this)

        mPresenter.initializeComponents(view)

        mPresenter.initializeRepository()

        setHasOptionsMenu(true)

        return view
    }

    /**
     * Checks if adapter was initialized
     *
     * @return Boolean
     */
    fun isAdapterInitialized(): Boolean
    {
        return this::adapter.isInitialized
    }

    /**
     * Actions on create options menu
     *      - adds search button
     *
     * @param menu
     * @param menuInflater
     *
     * @return Boolean
     */
    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        val searchButton = menu.findItem(R.id.action_search)
        val createButton = menu.findItem(R.id.action_create)

        checkButton = menu.findItem(R.id.action_check)
        checkButton.setOnMenuItemClickListener(mController.onCheckCategoriesMenuItemClicked())

        this.searchView.setMenuItem(searchButton)
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    override fun onBackPressed(): Boolean {
        return mController.onBackPressed()
    }
}