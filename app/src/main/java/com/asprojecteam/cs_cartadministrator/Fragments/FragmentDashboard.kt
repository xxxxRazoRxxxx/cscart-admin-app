package com.asprojecteam.cs_cartadministrator.Fragments


import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import android.widget.TextView
import com.asprojecteam.cs_cartadministrator.Controllers.DashboardController
import com.asprojecteam.cs_cartadministrator.MainActivity
import com.asprojecteam.cs_cartadministrator.Presenters.DashboardPresenter

import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.InterfaceManager.DetailedEntityInterfaceManager

/**
 * FragmentDashboard class
 */
class FragmentDashboard : Fragment() {

    lateinit var mPresenter: DashboardPresenter
    lateinit var mController: DashboardController

    lateinit var openOrdersNumberText: TextView
    lateinit var openOrdersNumberLoader: ProgressBar
    lateinit var ordersNumberText: TextView
    lateinit var ordersNumberLoader: ProgressBar
    lateinit var productsNumberText: TextView
    lateinit var productsNumberLoader: ProgressBar
    lateinit var outOfStockProductsNumberText: TextView
    lateinit var outOfStockProductsNumberLoader: ProgressBar
    lateinit var customersNumberText: TextView
    lateinit var customersNumberLoader: ProgressBar
    lateinit var categoriesNumberText: TextView
    lateinit var categoriesNumberLoader: ProgressBar

    var interfaceManager = DetailedEntityInterfaceManager()

    lateinit var mainContent: LinearLayout
    lateinit var progressBar: ProgressBar
    lateinit var viewerNoData: RelativeLayout

    /**
     * Actions on create View
     *  - creates controller
     *  - creates presenter
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_dashboard, container, false)

        mPresenter = DashboardPresenter(this)
        mController = DashboardController(this)

        mPresenter.initializeComponents(view)
        mPresenter.initializeController()

        return view
    }
}
