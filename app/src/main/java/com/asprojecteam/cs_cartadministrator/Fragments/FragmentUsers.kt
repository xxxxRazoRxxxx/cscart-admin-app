package com.asprojecteam.cs_cartadministrator.Fragments


import android.os.Bundle
import android.view.*
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import android.widget.RelativeLayout
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters.UsersAdapter
import com.asprojecteam.cs_cartadministrator.Controllers.UsersController
import com.asprojecteam.cs_cartadministrator.Presenters.UsersPresenter

import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.UserTypes
import com.asprojecteam.cs_cartadministrator.Tygh.Inrefaces.OnBackPressedListener
import com.asprojecteam.cs_cartadministrator.Tygh.InterfaceManager.ListPageInterfaceManager
import com.google.android.material.tabs.TabLayout
import com.miguelcatalan.materialsearchview.MaterialSearchView

/**
 * FragmentUsers class
 */
class FragmentUsers : Fragment(), OnBackPressedListener {
    /** Interface Manager */
    var interfaceManager = ListPageInterfaceManager()

    lateinit var mPresenter: UsersPresenter
    lateinit var mController: UsersController

    lateinit var viewer: RelativeLayout
    lateinit var viewerNoData: RelativeLayout
    lateinit var loader: ProgressBar

    lateinit var usersList: RecyclerView
    lateinit var usersCustomerAdapter: UsersAdapter
    lateinit var usersAdminAdapter: UsersAdapter
    lateinit var searchAdapter: UsersAdapter

    lateinit var usersTabLayout: TabLayout

    lateinit var searchView: MaterialSearchView
    lateinit var searchText: String

    lateinit var toolbar: Toolbar

    var currentList = UserTypes.CUSTOMER

    fun newInstance(): FragmentUsers {
        return FragmentUsers()
    }

    /**
     * Actions on create View
     *      - creates controller
     *      - creates presenter
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     *
     * @return View?
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_users, container, false)

        mPresenter = UsersPresenter(this)
        mController = UsersController(this)

        mPresenter.initializeComponents(view)
        mPresenter.initializeRepository()

        setHasOptionsMenu(true)

        return view
    }

    /**
     * Checks if users customer adapter is initialized
     *
     * @return Boolean
     */
    fun isUsersCustomerAdapterInitialized(): Boolean
    {
        return this::usersCustomerAdapter.isInitialized;
    }

    /**
     * Checks if users admin adapter is initialized
     *
     * @return Boolean
     */
    fun isUsersAdminAdapterInitialized(): Boolean
    {
        return this::usersAdminAdapter.isInitialized
    }

    /**
     * Actions on create options menu
     *      - adds search button
     *
     * @param menu
     * @param menuInflater
     *
     * @return Boolean
     */
    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        val searchButton = menu.findItem(R.id.action_search)
        this.searchView.setMenuItem(searchButton)
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    override fun onBackPressed(): Boolean {
        return mController.onBackPressed()
    }
}
