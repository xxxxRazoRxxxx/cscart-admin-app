package com.asprojecteam.cs_cartadministrator.Fragments


import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.asprojecteam.cs_cartadministrator.Controllers.LoginController
import com.asprojecteam.cs_cartadministrator.MainActivity

import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Presenters.LoginPresenter
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Login.Results
import com.asprojecteam.cs_cartadministrator.Tygh.Inrefaces.OnBackPressedListener
import com.google.android.material.bottomnavigation.BottomNavigationView

/**
 * FragmentLogin class
 */
class FragmentLogin : Fragment(), OnBackPressedListener {

    lateinit var mPresenter: LoginPresenter
    lateinit var mController: LoginController

    lateinit var loginProgressBar: ProgressBar
    lateinit var loginBtn: Button
    lateinit var loginDemoStoreBtn: Button
    lateinit var qrBtn: ImageButton
    lateinit var loginWebUrl: EditText
    lateinit var loginLogin: EditText
    lateinit var loginPassword: EditText
    lateinit var loginScrollView: ScrollView

    lateinit var mainActivity: MainActivity

    lateinit var bottomNavigationMenu: BottomNavigationView

    /**
     * Actions on create View
     *  - creates controller
     *  - creates presenter
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)

        mPresenter = LoginPresenter(this)
        mController = LoginController(this)

        mPresenter.initializeComponents(view)

        mPresenter.fillLogin()

        return view
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    override fun onBackPressed(): Boolean {
        return mController.onBackPressed()
    }
}