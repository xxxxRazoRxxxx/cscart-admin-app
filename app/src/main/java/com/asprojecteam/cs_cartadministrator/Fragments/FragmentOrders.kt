package com.asprojecteam.cs_cartadministrator.Fragments


import android.os.Bundle
import android.view.*
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import android.widget.RelativeLayout
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters.OrdersAdapter
import com.asprojecteam.cs_cartadministrator.Controllers.OrdersController
import com.asprojecteam.cs_cartadministrator.Presenters.OrdersPresenter

import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Inrefaces.OnBackPressedListener
import com.asprojecteam.cs_cartadministrator.Tygh.InterfaceManager.ListPageInterfaceManager
import com.miguelcatalan.materialsearchview.MaterialSearchView

/**
 * FragmentOrders class
 */
class FragmentOrders : Fragment(), OnBackPressedListener {
    /** Interface manager */
    var interfaceManager = ListPageInterfaceManager()

    lateinit var viewer: RelativeLayout
    lateinit var viewerNoData: RelativeLayout
    lateinit var loader: ProgressBar

    lateinit var ordersList: RecyclerView
    lateinit var ordersAdapter: OrdersAdapter
    lateinit var ordersSearchAdapter: OrdersAdapter

    lateinit var mPresenter: OrdersPresenter
    lateinit var mController: OrdersController

    lateinit var searchView: MaterialSearchView
    lateinit var searchText: String

    lateinit var ordersToolbar: Toolbar

    /**
     * Actions on create View
     *  - creates controller
     *  - creates presenter
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_orders, container, false)

        mPresenter = OrdersPresenter(this)
        mController = OrdersController(this)

        mPresenter.initializeComponents(view)
        mPresenter.initializeController()

        setHasOptionsMenu(true)

        return view
    }

    fun isOrdersAdapterInitialized(): Boolean
    {
        return this::ordersAdapter.isInitialized
    }

    fun isSearchViewInitialized(): Boolean
    {
        return this::searchView.isInitialized
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        val searchButton = menu.findItem(R.id.action_search)
        searchButton.title = resources.getString(R.string.order)
        this.searchView.setMenuItem(searchButton)
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    override fun onBackPressed(): Boolean {
        return mController.onBackPressed()
    }
}
