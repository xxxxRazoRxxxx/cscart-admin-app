package com.asprojecteam.cs_cartadministrator.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.Toolbar
import com.asprojecteam.cs_cartadministrator.Classes.OrderStatus
import com.asprojecteam.cs_cartadministrator.Controllers.OrderDetailedController
import com.asprojecteam.cs_cartadministrator.Presenters.OrderDetailedPresenter

import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Bundles.OrderBundle
import com.asprojecteam.cs_cartadministrator.Tygh.Inrefaces.OnBackPressedListener
import com.asprojecteam.cs_cartadministrator.Tygh.InterfaceManager.DetailedEntityInterfaceManager
import com.google.android.material.tabs.TabLayout

/**
 * FragmentOrderDetailed class
 */
class FragmentOrderDetailed : Fragment(), OnBackPressedListener {
    /** Interface manager */
    var interfaceManager = DetailedEntityInterfaceManager()

    lateinit var mPresenter: OrderDetailedPresenter
    lateinit var mController: OrderDetailedController

    lateinit var toolbar: Toolbar
    lateinit var loader: ProgressBar
    lateinit var viewer: RelativeLayout
    lateinit var viewerNoData: RelativeLayout

    lateinit var tabLayout: TabLayout
    lateinit var orderInformationBlock: ScrollView
    lateinit var paymentInformationBlock: ScrollView
    lateinit var paymentTable: TableLayout
    lateinit var paymentMethod: TextView
    lateinit var shippingTable: TableLayout
    lateinit var shippingMethod: TextView
    lateinit var customerInformationBlock: ScrollView
    lateinit var customerInformationTable: TableLayout
    lateinit var shippingAddressTable: TableLayout
    lateinit var billingAddressTable: TableLayout

    lateinit var updateStatusButton: Button
    lateinit var statusSelect: Spinner
    lateinit var productsTable: TableLayout
    lateinit var taxesTable: TableLayout
    lateinit var total: TextView
    lateinit var subtotal: TextView
    lateinit var shippingCost: TextView

    var orderStatusesList = mutableMapOf<String, OrderStatus>()
    var orderId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            orderId = it.getInt(OrderBundle.ORDER_ID)
        }
    }

    /**
     * Actions on create View
     *  - creates controller
     *  - creates presenter
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState:
        Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_order_detailed, container, false)

        mPresenter = OrderDetailedPresenter(this)
        mController = OrderDetailedController(this)

        mPresenter.initializeComponents(view)
        mPresenter.initializeController()

        return view
    }

    /**
     * Actions on Back button pressed
     *
     * @return Boolean
     */
    override fun onBackPressed(): Boolean {
        return mController.onBackPressed()
    }
}
