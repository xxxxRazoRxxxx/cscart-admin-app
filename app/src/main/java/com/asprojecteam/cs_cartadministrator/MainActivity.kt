package com.asprojecteam.cs_cartadministrator

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import com.asprojecteam.cs_cartadministrator.Classes.Product
import com.asprojecteam.cs_cartadministrator.Controllers.MainController
import com.asprojecteam.cs_cartadministrator.Presenters.MainPresenter
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.StoreEditions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {

    lateinit var mPresenter: MainPresenter
    lateinit var mController: MainController

    lateinit var leftSideBarMenu: RelativeLayout
    lateinit var bottomNavigationView: BottomNavigationView
    lateinit var sideBarMenu: NavigationView
    lateinit var drawerLayout: DrawerLayout

    lateinit var storeEditionTextView: TextView
    lateinit var addonStatusTextView: TextView

    var selectedCategories: MutableMap<Int, String> = mutableMapOf()
    var changedProduct: Product? = null

    var currentFragment = R.id.nav_home_menu

    var storeEdition = StoreEditions.CSCART
    var addonInstalled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        mPresenter = MainPresenter(this)
        mController = MainController(this)

        mPresenter.initializeComponents(this)
        mPresenter.openLoginPage()
    }

    override fun onBackPressed() {
        if (!mController.onBackPressed())
        {
            super.onBackPressed()
        }
    }
}