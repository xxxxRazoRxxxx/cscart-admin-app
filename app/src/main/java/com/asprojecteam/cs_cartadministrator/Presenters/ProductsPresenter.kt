package com.asprojecteam.cs_cartadministrator.Presenters

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.asprojecteam.cs_cartadministrator.Classes.Currency
import com.asprojecteam.cs_cartadministrator.Classes.Product
import com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters.ProductsAdapter
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentProducts
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.ParamIds

class ProductsPresenter(
    val mView: FragmentProducts
) {
    /**
     * Initializes necessary components for users fragment
     *
     * @param view - View of the fragment
     */
    fun initializeComponents(view: View)
    {
        mView.viewer = view.findViewById(R.id.products_viewer)
        mView.viewerNoData = view.findViewById(R.id.products_no_data_textview)
        mView.loader = view.findViewById(R.id.products_loader)

        mView.productsList = view.findViewById(R.id.products_list)

        mView.searchView = view.findViewById(R.id.products_search_view)
        mView.searchView.setOnQueryTextListener(mView.mController.onSearchQueryTextListener())
        mView.searchView.setOnSearchViewListener(mView.mController.onSearchViewListener())

        mView.toolbar = view.findViewById(R.id.products_toolbar)

        val llm = LinearLayoutManager(view.context)
        mView.productsList.layoutManager = llm

        if(mView.activity is AppCompatActivity){
            (mView.activity as AppCompatActivity).setSupportActionBar(mView.toolbar)
        }

        mView.interfaceManager.registrateComponents(
            mView.viewer,
            mView.viewerNoData,
            mView.loader,
            mView.searchView,
            mView.requireContext()
        )
    }

    fun initializeRepository()
    {
        //mView.mController.loadProducts()
        mView.mController.loadData()
    }

    fun showDefaultList()
    {
        mView.productsList.adapter = mView.adapter
        mView.interfaceManager.showList()
    }

    fun showProducts(
        productList: MutableList<Product>,
        params: LoadParameters,
        isSearch: Boolean,
        currency: Currency
    ) {
        val loadedProducts = params.page * params.itemsPerPage

        productList.forEach{
            it.currency = currency
        }

        if (loadedProducts <= params.totalItems!!) {
            var nextPageProduct = Product()
            nextPageProduct.id = ParamIds.NEXT_PAGE_ID
            productList.add(nextPageProduct)
        }

        if (isSearch) {
            if (params.page > 1) {
                mView.searchAdapter.addLoadedProducts(productList, params)
            } else {
                mView.searchAdapter =
                    ProductsAdapter(
                        productList,
                        params,
                        mView
                    )
                mView.productsList.adapter = mView.searchAdapter
            }
        } else {
            if (!mView.isAdapterInitialized()) {
                mView.adapter =
                    ProductsAdapter(
                        productList,
                        params,
                        mView
                    )
                mView.productsList.adapter = mView.adapter
            }

            if (params.page > 1) {
                mView.adapter.addLoadedProducts(productList, params)
            }
        }
        mView.interfaceManager.showList()
    }
}