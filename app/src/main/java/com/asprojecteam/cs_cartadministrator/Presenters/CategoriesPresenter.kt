package com.asprojecteam.cs_cartadministrator.Presenters

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.asprojecteam.cs_cartadministrator.Classes.Category
import com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters.CategoriesAdapter
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentCategories
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.ParamIds

class CategoriesPresenter (
    val mView: FragmentCategories
){
    /**
     * Initializes necessary components for users fragment
     *
     * @param view - View of the fragment
     */
    fun initializeComponents(view: View)
    {
        mView.viewer = view.findViewById(R.id.categories_viewer)
        mView.viewerNoData = view.findViewById(R.id.categories_no_data_textview)
        mView.loader = view.findViewById(R.id.categories_loader)

        mView.categoriesList = view.findViewById(R.id.categories_list)

        mView.searchView = view.findViewById(R.id.categories_search_view)
        mView.searchView.setOnQueryTextListener(mView.mController.onSearchQueryTextListener())
        mView.searchView.setOnSearchViewListener(mView.mController.onSearchViewListener())

        mView.toolbar = view.findViewById(R.id.categories_toolbar)
        mView.toolbar.title = mView.resources.getText(R.string.categories)

        val llm = LinearLayoutManager(view.context)
        mView.categoriesList.layoutManager = llm

        if(mView.activity is AppCompatActivity){
            (mView.activity as AppCompatActivity).setSupportActionBar(mView.toolbar)
        }

        mView.interfaceManager.registrateComponents(
            mView.viewer,
            mView.viewerNoData,
            mView.loader,
            mView.searchView,
            mView.requireContext()
        )
    }

    fun initializeRepository()
    {
        mView.mController.loadData()
    }

    fun showCategories(
        categoryList: MutableList<Category>,
        params: LoadParameters,
        isSearch: Boolean
    ) {
        val loadedCategories = params.page * params.itemsPerPage

        if (loadedCategories <= params.totalItems!!) {
            val nextPageItem = Category()
            nextPageItem.id = ParamIds.NEXT_PAGE_ID
            categoryList.add(nextPageItem)
        }

        if (isSearch) {
            if (params.page > 1) {
                mView.searchAdapter.addLoadedCategories(categoryList, params)
            } else {
                mView.searchAdapter =
                    CategoriesAdapter(
                        categoryList,
                        params,
                        mView
                    )
                mView.categoriesList.adapter = mView.searchAdapter
            }
        } else {
            if (!mView.isAdapterInitialized()) {
                mView.adapter =
                    CategoriesAdapter(
                        categoryList,
                        params,
                        mView
                    )
                mView.categoriesList.adapter = mView.adapter
            }

            if (params.page > 1) {
                mView.adapter.addLoadedCategories(categoryList, params)
            }
        }
        mView.interfaceManager.showList()
    }
}