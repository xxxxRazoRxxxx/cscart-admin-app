package com.asprojecteam.cs_cartadministrator.Presenters

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.asprojecteam.cs_cartadministrator.Classes.User
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentUserDetailed
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.DataLoader
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.UserStatuses

class UserDetailedPresenter (
    val mView: FragmentUserDetailed
) {
    private var dataLoader: DataLoader = DataLoader(mView.requireContext())

    /**
     * Initializes necessary components for product fragment
     *
     * @param view View of the fragment
     */
    fun initializeComponents(view: View)
    {
        mView.loader        = view.findViewById(R.id.user_detailed_loader)
        mView.viewer        = view.findViewById(R.id.user_detailed_viewer)
        mView.viewerNoData  = view.findViewById(R.id.user_detailed_no_data_textview)

        mView.toolbar       = view.findViewById(R.id.user_detailed_toolbar)
        mView.toolbar.title = mView.userName

        mView.userFirstNameEditText = view.findViewById(R.id.user_detailed_firstname)
        mView.userLastNameEditText  = view.findViewById(R.id.user_detailed_lastname)
        mView.userEmailEditText     = view.findViewById(R.id.user_detailed_email)
        mView.userPhoneEditText     = view.findViewById(R.id.user_detailed_phone)

        mView.userStatusRadioButtonActive = view.findViewById(R.id.radio_user_status_active)
        mView.userStatusRadioButtonActive.setOnClickListener(mView.mController.onStatusClicked())
        mView.userStatusRadioButtonDisabled = view.findViewById(R.id.radio_user_status_disabled)
        mView.userStatusRadioButtonDisabled.setOnClickListener(mView.mController.onStatusClicked())

        mView.interfaceManager.registrateComponents(
            mView.viewer,
            mView.viewerNoData,
            mView.loader,
            mView.requireContext()
        )

        if(mView.activity is AppCompatActivity){
            (mView.activity as AppCompatActivity).setSupportActionBar(mView.toolbar)
        }
    }

    /**
     * Initializes controller and load necessary information
     */
    fun initializeController()
    {
        mView.interfaceManager.showLoader()

        dataLoader.loadUserById(
            ::onLoadUserData,
            mView.interfaceManager::showNoDataError,
            mView.userId!!
        )
    }

    /**
     * Actions after user data was loaded successfully
     *
     * @param loadedUser Loaded data of the user
     */
    private fun onLoadUserData(loadedUser: User)
    {
        mView.interfaceManager.hideLoader()
        mView.interfaceManager.showViewer()

        mView.user = loadedUser

        mView.userFirstNameEditText.setText(loadedUser.firstname)
        mView.userLastNameEditText.setText(loadedUser.lastname)
        mView.userEmailEditText.setText(loadedUser.email)
        mView.userPhoneEditText.setText(loadedUser.phone)

        when(loadedUser.status) {
            UserStatuses.ACTIVE -> mView.userStatusRadioButtonActive.isChecked = true
            UserStatuses.DISABLED -> mView.userStatusRadioButtonDisabled.isChecked = true
        }
    }
}