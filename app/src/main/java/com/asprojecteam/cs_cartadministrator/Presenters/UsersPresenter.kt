package com.asprojecteam.cs_cartadministrator.Presenters

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters.UsersAdapter
import com.asprojecteam.cs_cartadministrator.Classes.User
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentUsers
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.ParamIds
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.UserTypes

/**
 * Presenter class of the FragmentUsers
 */
class UsersPresenter(
    val mView: FragmentUsers
) {
    /**
     * Initializes necessary components for users fragment
     *
     * @param view - View of the fragment
     */
    fun initializeComponents(view: View)
    {
        mView.viewer = view.findViewById(R.id.users_viewer)
        mView.viewerNoData = view.findViewById(R.id.users_no_data_textview)
        mView.loader = view.findViewById(R.id.users_loader)

        mView.usersList = view.findViewById(R.id.users_list)
        mView.usersTabLayout = view.findViewById(R.id.users_tablayout)
        mView.usersTabLayout.addOnTabSelectedListener(mView.mController.onTabLayoutSelected())

        mView.searchView = view.findViewById(R.id.users_search_view)
        mView.searchView.setOnQueryTextListener(mView.mController.onSearchQueryTextListener())
        mView.searchView.setOnSearchViewListener(mView.mController.onSearchViewListener())

        mView.toolbar = view.findViewById(R.id.users_toolbar)

        val llm = LinearLayoutManager(view.context)
        mView.usersList.layoutManager = llm

        if(mView.activity is AppCompatActivity){
            (mView.activity as AppCompatActivity).setSupportActionBar(mView.toolbar)
        }

        mView.interfaceManager.registrateComponents(
            mView.viewer,
            mView.viewerNoData,
            mView.loader,
            mView.searchView,
            mView.requireContext()
        )
    }

    /**
     * Initializes users controller.
     */
    fun initializeRepository()
    {
        mView.interfaceManager.showLoader()
        mView.mController.loadUsers(1, UserTypes.CUSTOMER)
        mView.mController.loadUsers(1, UserTypes.ADMIN)
    }

    /**
     * Show users list
     *
     * @param userList - List of the Users from the api
     * @param params   - Params of the founded userList
     */
    fun showUsers(userList: MutableList<User>, params: LoadParameters, isSearch: Boolean)
    {
        val loadedUsers = params.page * params.itemsPerPage

        if (loadedUsers <= params.totalItems!!) {
            val nextPageUser = User()
            nextPageUser.id = ParamIds.NEXT_PAGE_ID
            userList.add(nextPageUser)
        }

        if (isSearch) {
            if (params.page > 1) {
                mView.searchAdapter.addLoadedUsers(userList, params)
            } else {
                mView.searchAdapter =
                    UsersAdapter(
                        userList,
                        params,
                        mView
                    )
                mView.usersList.adapter = mView.searchAdapter
            }
        } else {
            if (params.userType == UserTypes.CUSTOMER) {
                if (!mView.isUsersCustomerAdapterInitialized()) {
                    mView.usersCustomerAdapter =
                        UsersAdapter(
                            userList,
                            params,
                            mView
                        )
                    mView.usersList.adapter = mView.usersCustomerAdapter
                }

                if (params.page > 1) {
                    mView.usersCustomerAdapter.addLoadedUsers(userList, params)
                }
            } else {
                if (!mView.isUsersAdminAdapterInitialized()) {
                    mView.usersAdminAdapter =
                        UsersAdapter(
                            userList,
                            params,
                            mView
                        )
                }

                if (params.page > 1) {
                    mView.usersAdminAdapter.addLoadedUsers(userList, params)
                }
            }
        }

        mView.interfaceManager.showList()
    }

    /**
     * Shows list of the Customers
     */
    fun showCustomerList()
    {
        mView.usersList.adapter = mView.usersCustomerAdapter
    }

    /**
     * Shows list of the Administrators
     */
    fun showAdminList()
    {
        mView.usersList.adapter = mView.usersAdminAdapter
    }
}