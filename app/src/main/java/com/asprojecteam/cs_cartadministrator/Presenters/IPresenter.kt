package com.asprojecteam.cs_cartadministrator.Presenters

import android.view.View

interface IPresenter {
    /**
     * Initialize components of the fragment
     *
     * @param view
     */
    fun initializeComponents(view: View)

    /**
     * Initialize controller
     */
    fun initializeController()
}