package com.asprojecteam.cs_cartadministrator.Presenters

import android.content.ContentValues.TAG
import android.graphics.Color
import android.graphics.Paint
import android.text.util.Linkify
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.asprojecteam.cs_cartadministrator.Classes.Currency
import com.asprojecteam.cs_cartadministrator.Classes.Order
import com.asprojecteam.cs_cartadministrator.Classes.OrderStatus
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentOrderDetailed
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters
import com.asprojecteam.cs_cartadministrator.Tygh.DataLoader
import com.asprojecteam.cs_cartadministrator.Tygh.InterfaceBuilder.LINEAR_LAYOUT
import com.asprojecteam.cs_cartadministrator.Tygh.InterfaceBuilder.TABLE_ROW_LAYOUT
import com.asprojecteam.cs_cartadministrator.Tygh.InterfaceBuilder.TableRowBuilder
import com.bumptech.glide.Glide


/**
 * Presenter class of the FragmentOrderDetailed
 */
class OrderDetailedPresenter (
    val mView: FragmentOrderDetailed
) {
    lateinit var currency: Currency
    lateinit var order: Order

    private lateinit var orderStatusesAdapter: ArrayAdapter<String>

    private var dataLoader: DataLoader = DataLoader(mView.requireContext())

    /**
     * Initializes necessary components for order fragment
     *
     * @param view View of the fragment
     */
    fun initializeComponents(view: View)
    {
        mView.loader       = view.findViewById(R.id.order_detailed_loader)
        mView.viewer       = view.findViewById(R.id.order_detailed_viewer)
        mView.viewerNoData = view.findViewById(R.id.order_detailed_no_data_textview)

        mView.updateStatusButton = view.findViewById(R.id.order_detailed_update_order_status)
        mView.updateStatusButton.setOnClickListener{ mView.mController.onUpdateStatusButtonClicked(order) }

        mView.tabLayout = view.findViewById(R.id.order_detailed_tablayout)
        mView.tabLayout.addOnTabSelectedListener(mView.mController.onTabLayoutSelected())

        mView.orderInformationBlock    = view.findViewById(R.id.order_detailed_order_information)
        mView.paymentInformationBlock  = view.findViewById(R.id.order_detailed_payment_information)
        mView.paymentTable             = view.findViewById(R.id.order_detailed_payment_information_table)
        mView.paymentMethod            = view.findViewById(R.id.order_detailed_payment_method)
        mView.shippingTable            = view.findViewById(R.id.order_detailed_shipping_information_table)
        mView.shippingMethod           = view.findViewById(R.id.order_detailed_shipping_method)
        mView.customerInformationBlock = view.findViewById(R.id.order_detailed_customer_information)
        mView.customerInformationTable = view.findViewById(R.id.order_detailed_customer_information_table)
        mView.shippingAddressTable     = view.findViewById(R.id.order_detailed_shipping_address_table)
        mView.billingAddressTable      = view.findViewById(R.id.order_detailed_billing_address_table)

        mView.statusSelect  = view.findViewById(R.id.order_detailed_status_select)
        mView.productsTable = view.findViewById(R.id.order_detailed_products_table)
        mView.taxesTable    = view.findViewById(R.id.order_detailed_taxes)
        mView.total         = view.findViewById(R.id.order_detailed_total_text)
        mView.subtotal      = view.findViewById(R.id.order_detailed_subtotal_text)
        mView.shippingCost  = view.findViewById(R.id.order_detailed_shipping_cost_text)

        mView.toolbar       = view.findViewById(R.id.order_detailed_toolbar)
        mView.toolbar.title = "${mView.resources.getString(R.string.order)} ${mView.orderId}"

        mView.interfaceManager.registrateComponents(
            mView.viewer,
            mView.viewerNoData,
            mView.loader,
            mView.requireContext()
        )

        if (mView.activity is AppCompatActivity){
            (mView.activity as AppCompatActivity).setSupportActionBar(mView.toolbar)
        }
    }

    /**
     * Initializes controller and load necessary information
     */
    fun initializeController()
    {
        mView.interfaceManager.showLoader()

        dataLoader.loadCurrency(::onLoadCurrency, ::onFailLoadOrder)
        dataLoader.loadOrderStatuses(::onLoadOrderStatuses, ::onFailLoadOrder, LoadParameters())
    }

    /**
     * Set status name to status spinner
     *
     * @param status Status description string
     */
    fun setStatusToOrderSelect(status: String)
    {
        mView.statusSelect.setSelection(orderStatusesAdapter.getPosition(status))
    }

    /**
     * Show order information block and hide others
     */
    fun showOrderInformationBlock()
    {
        mView.orderInformationBlock.visibility = View.VISIBLE
        mView.customerInformationBlock.visibility = View.GONE
        mView.paymentInformationBlock.visibility = View.GONE
    }

    /**
     * Show customer information block and hide others
     */
    fun showCustomerInformationBlock()
    {
        mView.orderInformationBlock.visibility = View.GONE
        mView.customerInformationBlock.visibility = View.VISIBLE
        mView.paymentInformationBlock.visibility = View.GONE
    }

    /**
     * Show payment information block and hide others
     */
    fun showPaymentInformationBlock()
    {
        mView.orderInformationBlock.visibility = View.GONE
        mView.customerInformationBlock.visibility = View.GONE
        mView.paymentInformationBlock.visibility = View.VISIBLE
    }

    /**
     * Actions after order data was loaded successfully
     *
     * @param loadedOrder Loaded data of the order
     */
    private fun onLoadOrderData(loadedOrder: Order?)
    {
        mView.interfaceManager.hideLoader();
        mView.interfaceManager.showViewer()

        mView.productsTable.removeAllViews()

        if (loadedOrder == null) {
            mView.interfaceManager.showNoDataError()
            return
        }

        order = loadedOrder

        order.currency = currency
        order.statusParams = mView.orderStatusesList[order.status]!!

        setStatusToOrderSelect(order.statusParams.description)

        mView.subtotal.text = order.getSubtotalWithCurrency()
        mView.shippingCost.text = order.getShippingCostWithCurrency()
        mView.total.text = order.getTotalWithCurrency()

        val rowBuilder = TableRowBuilder(mView.requireContext())

        val (rowSeparator, trParamsSep) = rowBuilder.getRowSeparator()
        mView.productsTable.addView(rowSeparator, trParamsSep)

        order.productGroups?.forEach let@{ it ->
            if (it.products === null) {
                return@let
            }

            it.products.forEach { (key, product) ->
                val productImage = rowBuilder.getImageElement(
                    mView.resources.getDimension(R.dimen.order_detailed_product_image_size).toInt()
                )
                val nameText = rowBuilder.getMainTextElement(
                    mView.resources.getDimension(R.dimen.order_detailed_product_name_size)
                )
                val amountText = rowBuilder.getTextElement(
                    mView.resources.getDimension(R.dimen.order_detailed_product_amount_size),
                    Gravity.CENTER,
                    LINEAR_LAYOUT
                )
                val totalText = rowBuilder.getTextElement(
                    mView.resources.getDimension(R.dimen.order_detailed_product_total_size),
                    Gravity.END,
                    LINEAR_LAYOUT
                )

                if (product.mainPair != null) {
                    Glide.with(mView.requireContext())
                        .load(product.mainPair!!.detailed.image_path)
                        .placeholder(R.drawable.ic_no_picture)
                        .into(productImage)
                }

                totalText.text = order.currency.getStringWithCurrencySymbol(order.products!![key]!!.subtotal)
                amountText.text = product.amount.toString()
                nameText.text = product.product

                val linearLayout = rowBuilder.getLinearLayout(
                    0,
                    mView.resources.getDimension(R.dimen.order_detailed_product_image_margin).toInt(),
                    mView.resources.getDimension(R.dimen.order_detailed_product_image_margin).toInt(),
                    mView.resources.getDimension(R.dimen.order_detailed_product_image_margin).toInt()
                )
                linearLayout.addView(productImage)
                linearLayout.addView(nameText)
                linearLayout.addView(amountText)
                linearLayout.addView(totalText)


                val (tableRow, tableRowLayoutParams) = rowBuilder.getRow()
                tableRow.addView(linearLayout)


                mView.productsTable.addView(tableRow, tableRowLayoutParams)

                val (rowSeparator2, trParamsSep2) = rowBuilder.getRowSeparator()
                mView.productsTable.addView(rowSeparator2, trParamsSep2)
            }
        }

        order.taxes!!.forEach {(_, tax) ->
            val taxText = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_taxes_name_size),
                Gravity.END,
                TABLE_ROW_LAYOUT
            )
            val taxSubtotal = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_taxes_name_size),
                Gravity.END,
                TABLE_ROW_LAYOUT
            )

            taxText.text = tax.getTaxString(mView.requireContext())
            taxSubtotal.text = order.currency.getStringWithCurrencySymbol(tax.taxSubtotal.toString())

            val (tableRow, tableRowLayoutParams) = rowBuilder.getRow()
            tableRow.let{
                it.addView(taxText)
                it.addView(taxSubtotal)
            }

            mView.taxesTable.addView(tableRow, tableRowLayoutParams)
        }

        showPaymentInformation(order, rowBuilder)
        showCustomerInformation(order, rowBuilder)
        showShippingInformation(order, rowBuilder)
        showBillingInformation(order, rowBuilder)
    }

    /**
     * Actions after currency was loaded successfully
     *
     * @param primaryCurrency Currency to show data with it
     */
    private fun onLoadCurrency(primaryCurrency: Currency)
    {
        currency = primaryCurrency

        dataLoader.loadOrderById(
            ::onLoadOrderData,
            mView.interfaceManager::showNoDataError,
            mView.orderId!!
        )
    }

    /**
     * Action after order statuses was loaded successfully
     *
     * @param orderStatusList Status list of the orders
     * @param loadParams      Params data
     */
    private fun onLoadOrderStatuses(orderStatusList: MutableList<OrderStatus>, loadParams: LoadParameters): Unit
    {
        val orderStatusesList = mutableMapOf<String, OrderStatus>()
        val statuses = mutableListOf<String>()

        orderStatusList.forEach{
            orderStatusesList[it.status] = it
            statuses.add(it.description)
        }

        mView.orderStatusesList = orderStatusesList

        orderStatusesAdapter = ArrayAdapter(mView.requireContext(),
            android.R.layout.simple_spinner_item, statuses)

        orderStatusesAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)

        mView.statusSelect.adapter = orderStatusesAdapter
    }

    /**
     * Shows payment information from the order data
     *
     * @param order      Order data
     * @param rowBuilder Builder to build interface elements
     */
    private fun showPaymentInformation(order: Order, rowBuilder: TableRowBuilder)
    {
        mView.paymentMethod.text = order.paymentMethod.toString()

        if (order.paymentInfo != null && order.paymentInfo.customerPhone != null) {
            val customerPhoneLabel = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            customerPhoneLabel.text = mView.resources.getString(R.string.customer_phone)

            val customerPhoneNumber = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.END,
                TABLE_ROW_LAYOUT
            )

            customerPhoneNumber.text = order.paymentInfo.customerPhone
            customerPhoneNumber.setLinkTextColor(mView.resources.getColor(R.color.colorBlue))
            customerPhoneNumber.paintFlags = Paint.UNDERLINE_TEXT_FLAG;
            Linkify.addLinks(customerPhoneNumber, Linkify.ALL)

            val (tableRow, tableRowLayoutParams) = rowBuilder.getRow()
            tableRow.let{
                it.addView(customerPhoneLabel)
                it.addView(customerPhoneNumber)
            }

            mView.paymentTable.addView(tableRow, tableRowLayoutParams)
        }

        if (order.shipping!![0].groupName != null) {
            val shippingGroupName = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            shippingGroupName.text = order.shipping[0].groupName

            val (tableRow, tableRowLayoutParams) = rowBuilder.getRow()
            tableRow.addView(shippingGroupName)

            mView.shippingTable.addView(tableRow, 0, tableRowLayoutParams)
        }

        mView.shippingMethod.text = order.shipping[0].shipping
    }

    /**
     * Shows customer information from order data
     *
     * @param order      Order data
     * @param rowBuilder Builder to build interface elements
     */
    private fun showCustomerInformation(order: Order, rowBuilder: TableRowBuilder)
    {
        val customerName = rowBuilder.getTextElement(
            mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
            Gravity.START,
            TABLE_ROW_LAYOUT
        )
        customerName.text = order.getName()

        val (customerNameTableRow, customerNameTableRowLayoutParams) = rowBuilder.getRow()
        customerNameTableRow.addView(customerName)

        val customerEmail = rowBuilder.getTextElement(
            mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
            Gravity.START,
            TABLE_ROW_LAYOUT,
            2
        )
        customerEmail.text = order.email
        customerEmail.setLinkTextColor(mView.resources.getColor(R.color.colorBlue))
        Linkify.addLinks(customerEmail, Linkify.ALL)

        val (customerEmailTableRow, customerEmailTableRowLayoutParams) = rowBuilder.getRow()
        customerEmailTableRow.addView(customerEmail)

        val customerIpLabel = rowBuilder.getTextElement(
            mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
            Gravity.START,
            TABLE_ROW_LAYOUT
        )
        customerIpLabel.text = mView.resources.getText(R.string.ip_address)

        val customerIp = rowBuilder.getTextElement(
            mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
            Gravity.END,
            TABLE_ROW_LAYOUT
        )
        customerIp.text = order.ipAddress

        val (customerIpTableRow, customerIpTableRowLayoutParams) = rowBuilder.getRow()
        customerIpTableRow.addView(customerIpLabel)
        customerIpTableRow.addView(customerIp)

        val customerPhoneLabel = rowBuilder.getTextElement(
            mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
            Gravity.START,
            TABLE_ROW_LAYOUT
        )
        customerPhoneLabel.text = mView.resources.getString(R.string.customer_phone)

        val customerPhone = rowBuilder.getTextElement(
            mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
            Gravity.END,
            TABLE_ROW_LAYOUT
        )

        customerPhone.text = order.phone
        customerPhone.setLinkTextColor(mView.resources.getColor(R.color.colorBlue))
        Linkify.addLinks(customerPhone, Linkify.ALL)

        val (customerPhoneTableRow, customerPhoneTableRowLayoutParams) = rowBuilder.getRow()
        customerPhoneTableRow.addView(customerPhoneLabel)
        customerPhoneTableRow.addView(customerPhone)

        mView.customerInformationTable.let{
            it.addView(customerNameTableRow, customerNameTableRowLayoutParams)
            it.addView(customerEmailTableRow, customerEmailTableRowLayoutParams)
            it.addView(customerIpTableRow, customerIpTableRowLayoutParams)
            it.addView(customerPhoneTableRow, customerPhoneTableRowLayoutParams)
        }
    }

    /**
     * Shows shipping information from order data
     *
     * @param order      Order data
     * @param rowBuilder Builder to build interface elements
     */
    private fun showShippingInformation(order: Order, rowBuilder: TableRowBuilder)
    {
        if (!order.shippingFirstname.isBlank() || !order.shippingLastname.isBlank()) {
            val shippingName = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            shippingName.text = order.getShippingName()

            val (shippingNameTableRow, shippingNameTableRowLayoutParams) = rowBuilder.getRow()
            shippingNameTableRow.addView(shippingName)

            mView.shippingAddressTable.addView(shippingNameTableRow, shippingNameTableRowLayoutParams)
        }

        if (!order.shippingAddress.isBlank()) {
            val shippingAddress = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            shippingAddress.text = order.shippingAddress

            val (shippingAddressTableRow, shippingAddressTableRowLayoutParams) = rowBuilder.getRow()
            shippingAddressTableRow.addView(shippingAddress)

            mView.shippingAddressTable.addView(shippingAddressTableRow, shippingAddressTableRowLayoutParams)
        }

        if (!order.shippingAddress2.isBlank()) {
            val shippingAddress2 = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            shippingAddress2.text = order.shippingAddress2

            val (shippingAddress2TableRow, shippingAddress2TableRowLayoutParams) = rowBuilder.getRow()
            shippingAddress2TableRow.addView(shippingAddress2)

            mView.shippingAddressTable.addView(shippingAddress2TableRow, shippingAddress2TableRowLayoutParams)
        }

        if (!order.shippingCity.isBlank()) {
            val shippingCity = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            shippingCity.text = order.shippingCity

            val (shippingCityTableRow, shippingCityTableRowLayoutParams) = rowBuilder.getRow()
            shippingCityTableRow.addView(shippingCity)

            mView.shippingAddressTable.addView(shippingCityTableRow, shippingCityTableRowLayoutParams)
        }

        if (!order.shippingState.isBlank()) {
            val shippingState = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            shippingState.text = order.getShippingStateString()

            val (shippingStateTableRow, shippingStateTableRowLayoutParams) = rowBuilder.getRow()
            shippingStateTableRow.addView(shippingState)

            mView.shippingAddressTable.addView(shippingStateTableRow, shippingStateTableRowLayoutParams)
        }

        if (!order.shippingZipcode.isBlank()) {
            val shippingZipcode = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            shippingZipcode.text = order.shippingZipcode

            val (shippingZipcodeTableRow, shippingZipcodeTableRowLayoutParams) = rowBuilder.getRow()
            shippingZipcodeTableRow.addView(shippingZipcode)

            mView.shippingAddressTable.addView(shippingZipcodeTableRow, shippingZipcodeTableRowLayoutParams)
        }

        if (!order.shippingCountry.isBlank()) {
            val shippingCountry = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            shippingCountry.text = order.shippingCountry

            val (shippingCountryTableRow, shippingCountryTableRowLayoutParams) = rowBuilder.getRow()
            shippingCountryTableRow.addView(shippingCountry)

            mView.shippingAddressTable.addView(shippingCountryTableRow, shippingCountryTableRowLayoutParams)
        }

        if (!order.shippingPhone.isBlank()) {
            val shippingPhone = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            shippingPhone.text = order.shippingPhone
            shippingPhone.setLinkTextColor(mView.resources.getColor(R.color.colorBlue))
            Linkify.addLinks(shippingPhone, Linkify.ALL)

            val (shippingPhoneTableRow, shippingPhoneTableRowLayoutParams) = rowBuilder.getRow()
            shippingPhoneTableRow.addView(shippingPhone)

            mView.shippingAddressTable.addView(shippingPhoneTableRow, shippingPhoneTableRowLayoutParams)
        }
    }

    /**
     * Shows billing information from order data
     *
     * @param order      Order data
     * @param rowBuilder Builder to build interface elements
     */
    private fun showBillingInformation(order: Order, rowBuilder: TableRowBuilder)
    {
        if (!order.billingFirstname.isBlank() || !order.billingLastname.isBlank()) {
            val billingName = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            billingName.text = order.getBillingName()

            val (billingNameTableRow, billingNameTableRowLayoutParams) = rowBuilder.getRow()
            billingNameTableRow.addView(billingName)

            mView.billingAddressTable.addView(billingNameTableRow, billingNameTableRowLayoutParams)
        }

        if (!order.billingAddress.isBlank()) {
            val billingAddress = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            billingAddress.text = order.billingAddress

            val (billingAddressTableRow, billingAddressTableRowLayoutParams) = rowBuilder.getRow()
            billingAddressTableRow.addView(billingAddress)

            mView.billingAddressTable.addView(billingAddressTableRow, billingAddressTableRowLayoutParams)
        }

        if (!order.billingAddress2.isBlank()) {
            val billingAddress2 = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            billingAddress2.text = order.billingAddress2

            val (billingAddress2TableRow, billingAddress2TableRowLayoutParams) = rowBuilder.getRow()
            billingAddress2TableRow.addView(billingAddress2)

            mView.billingAddressTable.addView(billingAddress2TableRow, billingAddress2TableRowLayoutParams)
        }

        if (!order.billingCity.isBlank()) {
            val billingCity = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            billingCity.text = order.billingCity

            val (billingCityTableRow, billingCityTableRowLayoutParams) = rowBuilder.getRow()
            billingCityTableRow.addView(billingCity)

            mView.billingAddressTable.addView(billingCityTableRow, billingCityTableRowLayoutParams)
        }

        if (!order.billingState.isBlank()) {
            val billingState = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            billingState.text = order.getBillingStateString()

            val (billingStateTableRow, billingStateTableRowLayoutParams) = rowBuilder.getRow()
            billingStateTableRow.addView(billingState)

            mView.billingAddressTable.addView(billingStateTableRow, billingStateTableRowLayoutParams)
        }

        if (!order.billingZipcode.isBlank()) {
            val billingZipcode = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            billingZipcode.text = order.billingZipcode

            val (billingZipcodeTableRow, billingZipcodeTableRowLayoutParams) = rowBuilder.getRow()
            billingZipcodeTableRow.addView(billingZipcode)

            mView.billingAddressTable.addView(billingZipcodeTableRow, billingZipcodeTableRowLayoutParams)
        }

        if (!order.billingCountry.isBlank()) {
            val billingCountry = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            billingCountry.text = order.billingCountry

            val (billingCountryTableRow, billingCountryTableRowLayoutParams) = rowBuilder.getRow()
            billingCountryTableRow.addView(billingCountry)

            mView.billingAddressTable.addView(billingCountryTableRow, billingCountryTableRowLayoutParams)
        }

        if (!order.billingPhone.isBlank()) {
            val billingPhone = rowBuilder.getTextElement(
                mView.resources.getDimension(R.dimen.order_detailed_payment_info_texts_size),
                Gravity.START,
                TABLE_ROW_LAYOUT
            )
            billingPhone.text = order.billingPhone
            billingPhone.setLinkTextColor(mView.resources.getColor(R.color.colorBlue))
            Linkify.addLinks(billingPhone, Linkify.ALL)

            val (billingPhoneTableRow, billingPhoneTableRowLayoutParams) = rowBuilder.getRow()
            billingPhoneTableRow.addView(billingPhone)

            mView.billingAddressTable.addView(billingPhoneTableRow, billingPhoneTableRowLayoutParams)
        }
    }

    /**
     * Actions after orders list data was failed to load
     */
    private fun onFailLoadOrder()
    {
        mView.interfaceManager.hideLoader()
        mView.interfaceManager.showNoDataError()
    }
}