package com.asprojecteam.cs_cartadministrator.Presenters

import android.view.View
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentDashboard
import com.asprojecteam.cs_cartadministrator.R

class DashboardPresenter(
    val mView: FragmentDashboard
): IPresenter {

    /**
     * Initializes necessary components for users fragment
     *
     * @param view - View of the fragment
     */
    override fun initializeComponents(view: View) {
        mView.openOrdersNumberText           = view.findViewById(R.id.number_of_open_orders)
        mView.openOrdersNumberLoader         = view.findViewById(R.id.number_of_open_orders_loader)
        mView.ordersNumberText               = view.findViewById(R.id.number_of_orders)
        mView.ordersNumberLoader             = view.findViewById(R.id.number_of_orders_loader)
        mView.productsNumberText             = view.findViewById(R.id.number_of_products)
        mView.productsNumberLoader           = view.findViewById(R.id.number_of_products_loader)
        mView.outOfStockProductsNumberText   = view.findViewById(R.id.number_of_out_of_stock_products)
        mView.outOfStockProductsNumberLoader = view.findViewById(R.id.number_of_out_of_stock_products_loader)
        mView.customersNumberText            = view.findViewById(R.id.number_of_customers)
        mView.customersNumberLoader          = view.findViewById(R.id.number_of_customers_loader)
        mView.categoriesNumberText           = view.findViewById(R.id.number_of_categories)
        mView.categoriesNumberLoader         = view.findViewById(R.id.number_of_categories_loader)
        mView.mainContent                    = view.findViewById(R.id.dashboard_main_content)
        mView.progressBar                    = view.findViewById(R.id.dashboard_progress_bar)
        mView.viewerNoData                   = view.findViewById(R.id.dashboard_no_data_textview)

        mView.interfaceManager.registrateComponents(
            mView.mainContent,
            mView.viewerNoData,
            mView.progressBar,
            mView.requireContext()
        )
    }

    override fun initializeController() {
        mView.mController.loadStatistic()
    }
}