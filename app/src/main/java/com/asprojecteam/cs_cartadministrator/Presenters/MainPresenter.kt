package com.asprojecteam.cs_cartadministrator.Presenters

import android.view.View
import androidx.drawerlayout.widget.DrawerLayout
import com.asprojecteam.cs_cartadministrator.Fragments.*
import com.asprojecteam.cs_cartadministrator.MainActivity
import com.asprojecteam.cs_cartadministrator.R

/**
 * Presenter class of the Main Activity
 */
class MainPresenter (
    private val mView: MainActivity
) {
    /**
     * Initializes necessary components for main activity
     *
     * @param activity Main activity activity
     */
    fun initializeComponents(activity: MainActivity)
    {
        mView.drawerLayout = activity.findViewById(R.id.activity_drawer)
        mView.leftSideBarMenu = activity.findViewById(R.id.left_menu)

        mView.bottomNavigationView = activity.findViewById(R.id.navigation)
        mView.bottomNavigationView.visibility = View.GONE

        mView.sideBarMenu = activity.findViewById(R.id.side_bar_menu)

        mView.bottomNavigationView.setOnNavigationItemSelectedListener { mView.mController.onBottomMenuSelected(it.itemId) }
        mView.sideBarMenu.setNavigationItemSelectedListener(mView.mController.onSideBarMenuItemSelected())

        mView.storeEditionTextView = mView.sideBarMenu.getHeaderView(0).findViewById(R.id.store_edition_text_view)
        mView.addonStatusTextView = mView.sideBarMenu.getHeaderView(0).findViewById(R.id.addon_status_text_view)

        closeSideBarMenu()
    }

    /**
     * Opens login page
     */
    fun openLoginPage()
    {
        mView.bottomNavigationView.visibility = View.GONE
        mView.supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, FragmentLogin())
            .commit()
    }

    /**
     * Close side bar menu and lock it
     */
    fun closeSideBarMenu()
    {
        mView.drawerLayout.closeDrawer(mView.leftSideBarMenu)
        mView.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, mView.leftSideBarMenu)
    }
}