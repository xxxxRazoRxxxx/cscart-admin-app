package com.asprojecteam.cs_cartadministrator.Presenters

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.asprojecteam.cs_cartadministrator.Classes.Currency
import com.asprojecteam.cs_cartadministrator.Classes.Image
import com.asprojecteam.cs_cartadministrator.Classes.Product
import com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters.ImagesAdapter
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentProductDetailed
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.DataLoader
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.ProductStatuses
import com.asprojecteam.cs_cartadministrator.Tygh.InterfaceBuilder.ItemElementWithRemoveButtonBuilder

class ProductDetailedPresenter (
    val mView: FragmentProductDetailed
) {
    private var dataLoader: DataLoader = DataLoader(mView.requireContext())

    /**
     * Initializes necessary components for product fragment
     *
     * @param view View of the fragment
     */
    fun initializeComponents(view: View)
    {
        val llm = LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false)

        mView.loader        = view.findViewById(R.id.product_detailed_loader)
        mView.viewer        = view.findViewById(R.id.product_detailed_viewer)
        mView.viewerNoData  = view.findViewById(R.id.product_detailed_no_data_textview)

        mView.productCodeEditText   = view.findViewById(R.id.product_detailed_code)
        mView.productCodeEditText.onFocusChangeListener = mView.mController.onProductCodeEditTextFocusChanged()
        mView.productNameEditText   = view.findViewById(R.id.product_detailed_name)
        mView.productNameEditText.onFocusChangeListener = mView.mController.onProductNameEditTextFocusChanged()
        mView.productPriceEditText  = view.findViewById(R.id.product_detailed_price)
        mView.productPriceEditText.onFocusChangeListener = mView.mController.onProductPriceEditTextFocusChanged()
        mView.productPriceLayout    = view.findViewById(R.id.product_detailed_price_layout)
        mView.productAmountEditText = view.findViewById(R.id.product_detailed_amount)
        mView.productAmountEditText.onFocusChangeListener = mView.mController.onProductAmountEditTextFocusChanged()

        mView.productStatusRadioButtonActive = view.findViewById(R.id.radio_product_status_active)
        mView.productStatusRadioButtonActive.setOnClickListener(mView.mController.onStatusClicked())
        mView.productStatusRadioButtonHidden = view.findViewById(R.id.radio_product_status_hidden)
        mView.productStatusRadioButtonHidden.setOnClickListener(mView.mController.onStatusClicked())
        mView.productStatusRadioButtonDisabled = view.findViewById(R.id.radio_product_status_disabled)
        mView.productStatusRadioButtonDisabled.setOnClickListener(mView.mController.onStatusClicked())

        mView.addCategoryButton = view.findViewById(R.id.product_detailed_add_category_button)
        mView.addCategoryButton.setOnClickListener(mView.mController.onAddCategoryButtonClicked())

        mView.productImageList = view.findViewById(R.id.product_detailed_image_list)
        mView.productImageList.layoutManager = llm

        mView.productCategoryList = view.findViewById(R.id.product_detailed_category_list)

        mView.toolbar = view.findViewById(R.id.product_detailed_toolbar)

        if (mView.productName.isEmpty()) {
            mView.toolbar.title = mView.resources.getText(R.string.product_new)
        } else {
            mView.toolbar.title = mView.productName
        }

        mView.interfaceManager.registrateComponents(
            mView.viewer,
            mView.viewerNoData,
            mView.loader,
            mView.requireContext()
        )

        if(mView.activity is AppCompatActivity){
            (mView.activity as AppCompatActivity).setSupportActionBar(mView.toolbar)
        }
    }

    /**
     * Shows products information
     *
     * @param product Product details
     */
    fun showProductInfo(product: Product, newInfo: Boolean = true)
    {
        mView.interfaceManager.hideLoader()
        mView.interfaceManager.showViewer()

        mView.productCategoryList.removeAllViews()

        if (product.categoryList.count() != 0) {
            val items: MutableMap<Int, String> = mutableMapOf()
            product.categoryList.forEach {
                items[it.id] = it.category
            }

            val itemsBuilder = ItemElementWithRemoveButtonBuilder(
                mView.requireContext(),
                mView.resources,
                items,
                mView.resources.getDimension(R.dimen.categories_name_in_list_text_size),
                mView.mController::onRemoveCategoryButtonClicked
            )

            val categoryElements = itemsBuilder.getElements()

            categoryElements.forEach {
                mView.productCategoryList.addView(it)
            }
        }

        mView.productCodeEditText.setText(product.code)
        mView.productNameEditText.setText(product.product)

        if (newInfo && product.product.isNotEmpty()) {
            mView.toolbar.title = product.product
        }

        mView.productPriceLayout.hint = "${mView.resources.getString(R.string.product_price)} (${mView.currency.getSymbol()})"
        mView.productPriceEditText.setText(product.price.toString())

        mView.productAmountEditText.setText(product.amount.toString())

        when(product.status) {
            ProductStatuses.ACTIVE -> mView.productStatusRadioButtonActive.isChecked = true
            ProductStatuses.HIDDEN -> mView.productStatusRadioButtonHidden.isChecked = true
            ProductStatuses.DISABLED -> mView.productStatusRadioButtonDisabled.isChecked = true
        }

        val productImages = mutableListOf<Image>()

        productImages.add(Image())

        if (product.mainPair != null) {
            productImages.add(product.mainPair!!.detailed)

            product.imagePairs.forEach { (_, value) ->
                if (value.detailed.relativePath != null) {
                    productImages.add(value.detailed)
                }
            }
        }

        val imagesAdapter =
            ImagesAdapter(
                productImages,
                mView
            )
        mView.productImageList.adapter = imagesAdapter
    }

    /**
     * Actions after product data was failed to load
     */
    fun onProductLoadFailed()
    {
        mView.interfaceManager.hideLoader()
        mView.interfaceManager.showNoDataError()
    }
}