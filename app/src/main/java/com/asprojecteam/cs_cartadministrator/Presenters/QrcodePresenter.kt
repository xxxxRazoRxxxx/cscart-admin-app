package com.asprojecteam.cs_cartadministrator.Presenters

import com.asprojecteam.cs_cartadministrator.Fragments.FragmentLogin
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentQrcode
import com.asprojecteam.cs_cartadministrator.R
import me.dm7.barcodescanner.zxing.ZXingScannerView
import android.os.Bundle
import android.Manifest.permission
import androidx.core.app.ActivityCompat
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat


class QrcodePresenter (
    private val mView: FragmentQrcode
) {
    private val REQUEST_CAMERA = 1

    fun openLoginPage(bundle: Bundle)
    {
        val fragmentLogin = FragmentLogin()
        fragmentLogin.arguments = bundle
        val fragmentTrans = mView.requireActivity().supportFragmentManager.beginTransaction()
        fragmentTrans.replace(R.id.main_container, fragmentLogin)
        fragmentTrans.commit()
    }

    fun getScannerView(): ZXingScannerView?
    {
        val permissionStatus = ContextCompat.checkSelfPermission(mView.requireContext(), permission.CAMERA)
        if (permissionStatus != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                mView.requireActivity(),
                arrayOf(permission.CAMERA),
                REQUEST_CAMERA
            )
        }

        val mScannerView = ZXingScannerView(mView.requireActivity().application)
        mScannerView.setAspectTolerance(0.5f)
        mScannerView.setResultHandler(mView)
        return mScannerView
    }
}