package com.asprojecteam.cs_cartadministrator.Presenters

import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.drawerlayout.widget.DrawerLayout
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentDashboard
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentLogin
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentQrcode
import com.asprojecteam.cs_cartadministrator.MainActivity
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Data.Saver
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Login.Results
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Saver.Types as SaverTypes

class LoginPresenter(
    val mView: FragmentLogin
) {
    /**
     * Initializes necessary components for users fragment
     *
     * @param view View of the fragment
     */
    fun initializeComponents(view: View)
    {
        mView.mainActivity      = mView.requireActivity() as MainActivity

        mView.loginProgressBar  = view.findViewById(R.id.login_progress_bar)
        mView.loginWebUrl       = view.findViewById(R.id.login_url)
        mView.loginLogin        = view.findViewById(R.id.login_login)
        mView.loginPassword     = view.findViewById(R.id.login_password)
        mView.loginBtn          = view.findViewById(R.id.login_button)
        mView.loginDemoStoreBtn = view.findViewById(R.id.login_demo_store_button)
        mView.qrBtn             = view.findViewById(R.id.login_qr_button)
        mView.loginScrollView   = view.findViewById(R.id.login_scroll_view)

        mView.bottomNavigationMenu = mView.requireActivity().findViewById(R.id.navigation)
        mView.bottomNavigationMenu.visibility = View.GONE

        mView.loginScrollView.isEnabled = false

        mView.loginBtn.setOnClickListener { mView.mController.onButtonLoginClick() }
        mView.loginDemoStoreBtn.setOnClickListener { mView.mController.onButtonLoginDemoStoreClick() }
        mView.qrBtn.setOnClickListener { mView.mController.onButtonQrClick() }
    }

    /**
     * Fills login inputs with login data
     */
    fun fillLogin()
    {
        if (mView.arguments != null) {
            val qrString = mView.requireArguments().getString(Results.RESULT)

            if (qrString != null) {
                val qrResult = mView.mController.parseQrCodeResult(qrString)
                if (qrResult.count() == 3) {
                    mView.loginPassword.setText("")
                    mView.loginLogin.setText("")
                    mView.loginWebUrl.setText("")

                    mView.loginPassword.setText(qrResult[0])
                    mView.loginLogin.setText(qrResult[1])
                    mView.loginWebUrl.setText(qrResult[2])
                } else {
                    showText("Incorrect QR code")
                }
            }
        } else {
            val saver = Saver(SaverTypes.SETTINGS_LOGIN, mView.requireContext())

            if (saver.restoreString(SaverTypes.LOGIN_KEY_API) != "") {
                mView.loginPassword.setText(saver.restoreString(SaverTypes.LOGIN_KEY_API)!!)
                mView.loginLogin.setText(saver.restoreString(SaverTypes.LOGIN_KEY_EMAIL)!!)
                mView.loginWebUrl.setText(saver.restoreString(SaverTypes.LOGIN_KEY_URL)!!)

                if (saver.restoreBoolean(SaverTypes.LOGIN_SUCCESSFUL) == true){
                    mView.mController.login()
                }
            }
        }
    }

    fun openHomePage()
    {
        val fragmentDashboard = FragmentDashboard()

        val fragmentTrans = mView.requireActivity().supportFragmentManager.beginTransaction()
        fragmentTrans.replace(R.id.main_container, fragmentDashboard)
        fragmentTrans.commit()
    }

    fun showProgressbar() {
        mView.loginProgressBar.visibility = View.VISIBLE
        mView.loginBtn.isEnabled = false
        mView.loginWebUrl.isEnabled = false
        mView.loginLogin.isEnabled = false
        mView.loginPassword.isEnabled = false
    }

    fun hideProgressbar()
    {
        mView.loginProgressBar.visibility = View.GONE
        mView.loginBtn.isEnabled = true
        mView.loginWebUrl.isEnabled = true
        mView.loginLogin.isEnabled = true
        mView.loginPassword.isEnabled = true
    }

    fun showBottomMenu()
    {
        mView.bottomNavigationMenu.visibility = View.VISIBLE
    }

    fun showText(message: String)
    {
        Toast.makeText(mView.context, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * Unlocks side bar menu
     */
    fun unlockSideBarMenu()
    {
        val drawer = mView.requireActivity().findViewById<DrawerLayout>(R.id.activity_drawer)
        val sideBarMenu = mView.requireActivity().findViewById<RelativeLayout>(R.id.left_menu)

        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, sideBarMenu)
    }
}