package com.asprojecteam.cs_cartadministrator.Presenters

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.asprojecteam.cs_cartadministrator.Classes.Order
import com.asprojecteam.cs_cartadministrator.Classes.OrderStatus
import com.asprojecteam.cs_cartadministrator.Tygh.RVAdapters.OrdersAdapter
import com.asprojecteam.cs_cartadministrator.Fragments.FragmentOrders
import com.asprojecteam.cs_cartadministrator.Tygh.Enum.Common.ParamIds
import com.asprojecteam.cs_cartadministrator.R
import com.asprojecteam.cs_cartadministrator.Tygh.Api.LoadParameters

/**
 * Class OrdersPresenter
 */
class OrdersPresenter(
    val mView: FragmentOrders
): IPresenter {
    /**
     * Initializes necessary components for users fragment
     *
     * @param view View of the fragment
     */
    override fun initializeComponents(view: View)
    {
        val llm = LinearLayoutManager(view.context)

        mView.ordersList = view.findViewById(R.id.orders_list)
        mView.viewer = view.findViewById(R.id.orders_viewer)
        mView.viewerNoData = view.findViewById(R.id.orders_no_data_textview)

        mView.loader = view.findViewById(R.id.orders_loader)

        mView.searchView = view.findViewById(R.id.orders_search_view)
        mView.searchView.setOnQueryTextListener(mView.mController.onOrdersSearchQueryTextListener())
        mView.searchView.setOnSearchViewListener(mView.mController.onOrdersSearchViewListener())

        mView.ordersToolbar = view.findViewById(R.id.orders_toolbar)

        mView.ordersList.layoutManager = llm

        if(mView.activity is AppCompatActivity){
            (mView.activity as AppCompatActivity).setSupportActionBar(mView.ordersToolbar)
        }

        mView.interfaceManager.registrateComponents(
            mView.viewer,
            mView.viewerNoData,
            mView.loader,
            mView.searchView,
            mView.requireContext()
        )
    }

    override fun initializeController()
    {
        mView.interfaceManager.showLoader()
        mView.mController.loadOrders()
    }

    fun showDefaultOrderList()
    {
        mView.ordersList.adapter = mView.ordersAdapter
        mView.interfaceManager.showList()
    }

    fun showSearchOrder(
        order: Order,
        orderStatusesList: MutableMap<String, OrderStatus>
    ) {
        if (orderStatusesList[order.status] == null) {
            mView.interfaceManager.showNoDataError()
            return
        }

        order.currency = mView.mController.currency

        order.statusParams = orderStatusesList[order.status]!!

        val params = LoadParameters()

        mView.ordersSearchAdapter =
            OrdersAdapter(
                mutableListOf(order),
                params,
                mView
            )
        mView.ordersList.adapter = mView.ordersSearchAdapter
        mView.interfaceManager.showList()
    }

    fun showOrders(
        orderList: MutableList<Order>,
        params: LoadParameters,
        orderStatusesList: MutableMap<String, OrderStatus>
    ) {
        mView.interfaceManager.showList()

        orderList.forEach{
            it.statusParams = orderStatusesList[it.status]!!
            it.currency = mView.mController.currency
        }

        val loadedOrders = params.page * params.itemsPerPage

        if (loadedOrders <= params.totalItems!!) {
            val nextPageOrder = Order()
            nextPageOrder.id = ParamIds.NEXT_PAGE_ID
            orderList.add(nextPageOrder)
        }

        if (!mView.isOrdersAdapterInitialized()) {
            mView.ordersAdapter =
                OrdersAdapter(
                    orderList,
                    params,
                    this.mView
                )
            mView.mPresenter.showDefaultOrderList()
        }

        if (params.page > 1) {
            mView.ordersAdapter.addLoadedOrders(orderList, params)
        }
    }
}